<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'auth' => [
            'class' => 'common\modules\auth\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['']
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
            'class' => 'frontend\vendor\ApiErrorHandler',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'POST register' => 'login/register',
                'POST sign-in-facebook' => 'login/login-facebook',
                'POST sign-in' => 'login/login',
                'POST request-password-reset' => 'login/request-password-reset',
                'POST reset-password' => 'login/reset-password',
                'POST dive' => 'dives/create',
                'GET tester' => 'tester/index',
                'GET facebook' => 'tester/facebook',
                'GET admin/login' => 'admin/login',
                'GET admin/unmoderated-spots' => 'admin-dive-spots/unmoderated-list',
                'PUT admin/unmoderated-spots/<id:\d+>' => 'admin-dive-spots/unmoderated-accept',
                'GET admin/dive-spots' => 'admin-dive-spots/index',
                'GET dive-spots' => 'dive-spots/index',
                'GET dive-spots/synchronize/<synchronize_time:\d+>' => 'dive-spots/synchronize',
                'GET dives/synchronize/<synchronize_time:\d+>' => 'dives/synchronize',
                'DELETE dive/<id:\d+>' => 'dives/delete',
                'POST dive/<id:\d+>' => 'dives/update',
                'POST dive/share/<id:\d+>' => 'dives/share',
                'POST post' => 'posts/create',
                'GET posts/<user_id:\d+>' => 'posts/index',
                'DELETE post/<id:\d+>' => 'posts/delete',
                'POST comment' => 'comments/create',
                'GET post/<post_id:\d+>/comments' => 'comments/index',
                'DELETE comment/<id:\d+>' => 'comments/delete',
                'POST like' => 'likes/create',
                'DELETE post/<post_id:\d+>/like' => 'likes/delete',
                'POST user/settings' => 'users/save-settings',
                'GET user/settings' => 'users/get-settings',
                'GET users' => 'users/index',
                'POST user/<user_id:\d+>/subscribe' => 'followers/subscribe',
                'POST user/<user_id:\d+>/unsubscribe' => 'followers/unsubscribe',
                'GET subscriptions' => 'subscriptions/index',
                'GET users/top' => 'users/top',
                'POST user/attach-facebook-login' => 'users/attach-facebook',
                'POST user/detach-facebook-login' => 'users/detach-facebook',
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
    ],
    'timeZone' => 'Europe/Kiev',
    'params' => $params,

];
