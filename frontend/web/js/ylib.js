$(document).on('submit','.ajax-form', function(e) {
    e.preventDefault();
    ajaxFormSubmit($(this))
});

function ajaxFormSubmit(form) {
    var $$ = form;


    var data = $$.serialize(),
        action = $$.attr('action'),
        method = $$.attr('method'),
        callback = $$.data('callback'),
        before_function = $$.data('before-action'),
        object = $$.data('object');

    if (!!before_function) {
        object ? eval(before_function)($$) : eval(before_function)();
    }

    $.ajax({
        type:method,
        url:action,
        data:data,
        success:function(response) {
            if(response.status===0){
                console.log(response);
            } else {
                if (!!callback)
                    object ? eval(callback)(response, $$) : eval(callback)(response);
            }
        }
    });
}
