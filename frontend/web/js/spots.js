function setWaitStatus(form) {
    form.find('button[type=submit]').button('loading');
}

function removeSpot(response, form) {
    var $btn = form.find('button[type=submit]');
    if(response.success) {
        $btn.text('Success');
        $btn.addClass('btn-success');
        setTimeout(function() {
            $btn.closest('form').closest('.row').fadeOut(500, function() {
                $(this).remove();
            });
        }, 1000);
    } else {
        $btn.text('Error');
        $btn.addClass('btn-warning');
        setTimeout(function() {
            $btn.button('reset').removeClass('btn-warning').text('Accept');
        }, 2000);
    }
}

$(document).on('focusout', '.ui-autocomplete-input', function() {
    var $$ = $(this),
        target_input = $(this).siblings('input[name=original-id]'),
        inputs = $$.closest('form').find('input[type=text], input[type=checkbox]').not($$),
        value = $$.data('prev-value') === $$.val() ? $$.data('original-id') : '';

    target_input.val(value);
    if(!value) {
        inputs.attr('readonly', false);
        inputs.removeClass('ignore-helpers').siblings('p').removeClass('hidden');
        $$.val('');
    }
});