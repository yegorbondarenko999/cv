<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "dives".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $spot_id
 * @property string $created_at
 * @property string $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'dive_id'], 'unique', 'targetAttribute' => ['owner_id', 'dive_id'], 'filter'=>['not', ['dive_id' => null]], 'message' => 300001],
            ['note', 'trim'],
            ['note', 'string', 'message' => 300005, 'min'=>1, 'max' => 65000, 'tooShort'=>300002, 'tooLong' => 300003],
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function fields()
    {
        $fields =  [
            'id' => function() {
                return (int)$this->id;
            },
            'note',
            'created_at',
        ];

        return $fields;
    }

    public function extraFields()
    {
        return [
            'image' => function() {
                $image = isset($this->images) ? 'www.'.Yii::$app->params['domainName'].'/'.$this->images[0]->url : null;
                return $image;
            },
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    public function getDive()
    {
        return $this->hasOne(Dive::className(), ['id' => 'dive_id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['post_id' => 'id']);
    }

    public function getImages()
    {
        return $this->hasMany(PostsImage::className(), ['post_id' => 'id']);
    }
}
