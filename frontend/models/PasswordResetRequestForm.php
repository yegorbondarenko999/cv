<?php
namespace frontend\models;

use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    private $registration_type = 'conventional';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'emailTrim' => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required', 'message' => 100001],
            'emailPattern' => ['email', 'email', 'message' => 100002],
            'emailLength' => ['email', 'string', 'message' => 100003, 'min' => 5, 'max' => 100, 'tooShort' => 100004, 'tooLong' => 100005],
            'emailExist' => ['email', 'emailExist'],
        ];
    }

    public function emailExist()
    {
        if(!User::findOne(['login' => $this->email, 'registration_type' => $this->registration_type]))
            $this->addError('email', 100025);
    }
}
