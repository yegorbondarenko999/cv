<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class AdminLoginForm extends Model
{
    public $login;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // login and password are both required
            [['login', 'password'], 'required'],
            ['login', 'checkAdminLogin'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getAdmin();
            // yes, it's to funny... need to think
            $user->setPassword(base64_encode($this->password));
            if (!$user || !$user->validatePassword()) {
                $this->addError($attribute, 'Incorrect login or password.');
            }
        }
    }

    /** Not use compare validator, because js validator - not good idea))
     */
    public function checkAdminLogin()
    {
        if($this->login !== 'lol@lol.lol')
            $this->addError('login', 'Incorrect login or password.');
    }

    /**
     * Logs in a user using the provided login and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getAdmin(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[login]]
     *
     * @return User|null
     */
    protected function getAdmin()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['login' => 'lol@lol.lol']);
        }
        return $this->_user;
    }
}
