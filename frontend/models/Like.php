<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "likes".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $author_id
 * @property string $created_at
 * @property string $updated_at
 */
class Like extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['post_id', 'required', 'message' => 300012],
            ['post_id', 'exist', 'targetClass'=>'frontend\models\Post', 'targetAttribute'=>'id', 'message'=>300006],
            ['post_id', 'likeUnique'],
        ];
    }

    public function likeUnique()
    {
        if(Like::find()->where(['post_id' => $this->post_id, 'author_id' => Yii::$app->user->identity->id])->one())
            $this->addError('number', 300015);
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
