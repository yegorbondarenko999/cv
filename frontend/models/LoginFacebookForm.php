<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * LoginFacebook form
 */
class LoginFacebookForm extends Model
{
    public $access_token;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'accessTokenTrim' => ['access_token', 'filter', 'filter' => 'trim'],
            'accessTokenRequired' => ['access_token', 'required', 'message' => 100021],
        ];
    }
}
