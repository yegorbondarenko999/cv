<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $author_id
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['post_id', 'required', 'message' => 300012, 'on'=>'create'],
            ['post_id', 'exist', 'targetClass'=>'frontend\models\Post', 'targetAttribute'=>'id', 'message'=>300006],
            ['body', 'trim'],
            ['body', 'required', 'message' => 300008, 'on'=>'create'],
            ['body', 'string', 'message' => 300009, 'min'=>1, 'max' => 65000, 'tooShort'=>300010, 'tooLong' => 300011],
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function fields()
    {
        $fields =  [
            'id' => function() {
                return (int)$this->id;
            },
            'body',
            'created_at',
        ];

        return $fields;
    }

    public function extraFields()
    {
        return [
            'author' => function() {
                return $this->author;
            }
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
