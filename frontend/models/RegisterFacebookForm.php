<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * RegisterFacebook form
 */
class RegisterFacebookForm extends Model
{
    public $login;
    private $registration_type = 'facebook';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'loginTrim' => ['login', 'filter', 'filter' => 'trim'],
            'loginRequired' => ['login', 'required', 'message' => 100013],
            'loginLength' => ['login', 'string', 'message' => 100014, 'min' => 1, 'max' => 20, 'tooShort' => 100015, 'tooLong' => 100016],
            'loginUnique' => ['login', 'loginUnique'],
        ];
    }

    public function loginUnique()
    {
        if(User::findOne(['login' => $this->login, 'registration_type' => $this->registration_type]))
            $this->addError('login', 100017);
    }
}
