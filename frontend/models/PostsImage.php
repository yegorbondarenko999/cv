<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "posts_images".
 *
 * @property integer $id
 * @property integer $dive_id
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 *
 */
class PostsImage extends \yii\db\ActiveRecord
{
    use \frontend\vendor\Saveble;

    //original file, uploaded by user
    public $file = null;

    protected $public_directory = 'posts_images';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'url'], 'required', 'message' => 300017],
            [['post_id'], 'integer', 'message' => 300018],
            [['url'], 'string', 'max' => 255, 'message' => 300019],
            [['url'], 'unique', 'message' => 300020],
            ['file', 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'mimeTypes' => ["image/gif", "image/pjpeg", "image/jpeg", "image/jpg", "image/png", "image/x-png"],  'message' => 300021, 'wrongExtension' => 300022, 'wrongMimeType' => 300023, 'on' => 'save_users_image'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [],
            'save_users_image' => ['file'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    public function upload()
    {
        $this->scenario = 'save_users_image';

        if(!$this->validate())
            return false;

        if(!($this->url = $this->saveFile($this->file))) {
            $this->deleteFile();
            $this->addError('posts_file', 300024);
            return false;
        }

        return true;
    }



    public function fields() {
        return [
            'id',
            'url' => function() {
                return 'www.'.Yii::$app->params['domainName'].'/'.$this->url;
            }
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function beforeDelete() {
        $this->deleteFile();
        return parent::beforeDelete();
    }

    public function deleteFile() {
        $url_start = substr($this->url, 0, strripos($this->url,'.'));
        $url_end = substr($this->url, strripos($this->url,'.'));

        $exemplar = $url_start.$url_end;

        if(file_exists($exemplar))
            unlink($exemplar);
    }
}
