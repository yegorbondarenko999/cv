<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "dives_images".
 *
 * @property integer $id
 * @property integer $dive_id
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 *
 */
class DivesImage extends \yii\db\ActiveRecord
{
    use \frontend\vendor\Saveble;

    //original file, uploaded by user
    public $file = null;

    protected $public_directory = 'dives_images';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dives_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dive_id', 'url'], 'required', 'message' => 200031],
            [['dive_id'], 'integer', 'message' => 200032],
            [['url'], 'string', 'max' => 255, 'message' => 200033],
            [['url'], 'unique', 'message' => 200034],
            ['file', 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'mimeTypes' => ["image/gif", "image/pjpeg", "image/jpeg", "image/jpg", "image/png", "image/x-png"],  'message' => 200035, 'wrongExtension' => 200036, 'wrongMimeType' => 200037, 'on' => 'save_users_image'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [],
            'save_users_image' => ['file'],
        ];

    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dive_id' => 'Dive ID',
            'url' => 'Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDive()
    {
        return $this->hasOne(Dive::className(), ['id' => 'dive_id']);
    }

    public function upload()
    {
        $this->scenario = 'save_users_image';

        if(!$this->validate())
            return false;

        if(!($this->url = $this->saveFile($this->file))) {
            $this->deleteFile();
            $this->addError('avatar_file', 200074);
            return false;
        }
        return true;
    }



    public function fields() {
        return [
            'id',
            'url' => function() {
                return 'www.'.Yii::$app->params['domainName'].'/'.$this->url;
            }
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function beforeDelete() {
        $this->deleteFile();
        return parent::beforeDelete();
    }

    public function deleteFile() {
        $url_start = substr($this->url, 0, strripos($this->url,'.'));
        $url_end = substr($this->url, strripos($this->url,'.'));
//
//        foreach ($this->sizeset as $size) {
//
//
//            $photo_size_exemplar = public_path().'/files/images/'.$url_start.$size['postfix'].$url_end;
//
//            if(file_exists($photo_size_exemplar))
//                unlink($photo_size_exemplar);
//        }

        //$exemplar = $url_start.'_original'.$url_end;
        $exemplar = $url_start.$url_end;

        if(file_exists($exemplar))
            unlink($exemplar);
    }
}
