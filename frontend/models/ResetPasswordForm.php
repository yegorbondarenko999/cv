<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $reset_token;
    private $password;
    private $registration_type = 'conventional';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'resetTokenTrim' => ['reset_token', 'filter', 'filter' => 'trim'],
            'resetTokenRequired' => ['reset_token', 'required', 'message' => 100027],
            'resetTokenLength' => ['reset_token', 'string', 'min' => 36, 'max' => 50, 'message' => 100026, 'tooShort' => 100026, 'tooLong' => 100026],
            'resetTokenExist' => ['reset_token', 'resetTokenExistValid'],

            'passwordRequired' => ['password', 'required', 'message' => 100007],
            'passwordLength' => ['password', 'string', 'min' => 6, 'max' => 100, 'message' => 100008, 'tooShort' => 100009, 'tooLong' => 100010],
        ];
    }

    public function resetTokenExistValid()
    {
        $user = User::findOne(['password_reset_token' => $this->reset_token, 'registration_type' => $this->registration_type]);

        if(!$user || !$user->isPasswordResetTokenValid())
            $this->addError('email', 100026);
    }

    public function setPassword($password)
    {
        $this->password = base64_decode($password);
    }

    public function getPassword()
    {
        return $this->password;
    }
}
