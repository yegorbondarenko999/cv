<?php

namespace frontend\models;

use Yii;
use frontend\vendor\ErrorsHelper;
use yii\db\Expression;

/**
 * This is the model class for table "dive_spots".
 *
 * @property integer $id
 * @property string $country
 * @property string $region
 * @property string $location
 * @property string $spot_name
 * @property string $lat
 * @property string $lng
 * @property string $created_at
 * @property string $updated_at
 */
class DiveSpot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dive_spots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ErrorsHelper::setRulesTranslate([
            ['country', 'required', 'message' => 200039],
            ['country', 'string', 'skipOnEmpty' => false, 'message' => 200040, 'min' => 3, 'max' => 50, 'tooShort' => 200041, 'tooLong' => 200042],

            ['region', 'required', 'message' => 200043],
            ['region', 'string', 'skipOnEmpty' => false, 'message' => 200044, 'min' => 3, 'max' => 50, 'tooShort' => 200045, 'tooLong' => 200046],

            ['location', 'required', 'message' => 200047],
            ['location', 'string', 'skipOnEmpty' => false, 'message' => 200048, 'min' => 3, 'max' => 50, 'tooShort' => 200049, 'tooLong' => 200050],

            ['spot_name', 'required', 'message' => 200051],
            ['spot_name', 'string', 'skipOnEmpty' => false, 'message' => 200052, 'min' => 3, 'max' => 50, 'tooShort' => 200053, 'tooLong' => 200054],

            ['global', 'boolean'],

            ['lat', 'required', 'message' => 200055],
            ['lat', 'number', 'integerOnly' => false, 'message' => 200056, 'min' => -90, 'max' => 90, 'tooSmall' => 200057, 'tooBig' => 200058],

            ['lng', 'required', 'message' => 200059],
            ['lng', 'number', 'integerOnly' => false, 'message' => 200060, 'min' => -180, 'max' => 180, 'tooSmall' => 200061, 'tooBig' => 200062],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'region' => 'Region',
            'location' => 'Location',
            'spot_name' => 'Spot Name',
            'global' => 'Global',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }

    public function fields()
    {
        $fields = [
            "id",
            "country",
            "region",
            "location",
            "spot_name",
            "lat" => function() {
                return round($this->lat, 4);
            },
            "lng" => function() {
                return round($this->lng, 4);
            },
        ];

        if($this->scenario === 'synchronize')
            $fields['deleted'] = function() {
                return (bool)$this->deleted;
            };

        return $fields;
    }
}
