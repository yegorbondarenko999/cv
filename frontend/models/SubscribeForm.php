<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class SubscribeForm extends Model
{
    public $user_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'required', 'message' => 500001],
            ['user_id', 'targetExist'],
            ['user_id', 'uniqueFollower'],
        ];
    }

    public function targetExist() {
        if(!User::find()
                ->where(['id'=>$this->user_id])
                ->andWhere(['!=', 'id', Yii::$app->user->identity->id])
                ->one()
        )
            $this->addError('number', 500002);
    }

    public function uniqueFollower() {
        if(Yii::$app->user->identity->getSubscriptions()->where(['id' => $this->user_id])->one())
            $this->addError('number', 500003);
    }

}
