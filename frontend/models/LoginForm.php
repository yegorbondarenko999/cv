<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    private $password;
    private $registration_type = 'conventional';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'emailTrim' => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required', 'message' => 100001],
            'emailPattern' => ['email', 'email', 'message' => 100002],
            'emailLength' => ['email', 'string', 'message' => 100003, 'min' => 5, 'max' => 100, 'tooShort' => 100004, 'tooLong' => 100005],
            'emailExist' => ['email', 'emailExist'],

            'passwordRequired' => ['password', 'required', 'message' => 100007],
            'passwordLength' => ['password', 'string', 'min' => 6, 'max' => 100, 'message' => 100008, 'tooShort' => 100009, 'tooLong' => 100010],
        ];
    }

    public function emailExist()
    {
        if(!User::findOne(['login' => $this->email, 'registration_type' => $this->registration_type]))
            $this->addError('email', 100023);
    }

    public function setPassword($password)
    {
        $this->password = base64_decode($password);
    }

    public function getPassword()
    {
        return $this->password;
    }
}
