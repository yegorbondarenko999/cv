<?php
namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;

/**
 * User model
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    use \frontend\vendor\Saveble;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    private $password;
    public $avatar_file;
    protected $public_directory = 'avatars';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'passwordCheck' => ['password', 'validatePassword', 'on' => 'login'],

            ['name', 'required', 'message' => 400001],
            ['name', 'trim'],
            ['name', 'string', 'message' => 400002, 'min' => 1, 'max' => 50, 'tooShort' => 400003, 'tooLong' => 400004],

            ['surname', 'required', 'message' => 400011],
            ['surname', 'trim'],
            ['surname', 'string', 'message' => 400012, 'min' => 1, 'max' => 50, 'tooShort' => 400013, 'tooLong' => 4000014],

            ['date_of_birth', 'date', 'format' => 'yyyy-M-d', 'message' => 400005],

            ['certificate', 'trim'],
            ['certificate', 'string', 'message' => 400006, 'min' => 10, 'max' => 30, 'tooShort' => 400007, 'tooLong' => 400008],

            ['note', 'trim'],
            ['note', 'string', 'message' => 400009, 'max' => 65000, 'tooLong' => 400010],

            ['language', 'required', 'message' => 400015],
            ['language', 'in', 'range' => ['eng', 'rus', 'ukr'], 'message' => 400016],

            ['measurement_system', 'required', 'message' => 400017],
            ['measurement_system', 'in', 'range' => ['metric', 'imperial'], 'message' => 400018],

            ['starting_dives_number', 'required', 'message' => 400019],
            ['starting_dives_number', 'number', 'integerOnly' => true, 'message' => 400020, 'min' => 1, 'max' => 999999999, 'tooSmall' => 400021, 'tooBig' => 400022],

            ['avatar', 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'mimeTypes' => ["image/gif", "image/pjpeg", "image/jpeg", "image/jpg", "image/png", "image/x-png"],  'message' => 400011, 'wrongExtension' => 400012, 'wrongMimeType' => 400013],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public function isPasswordResetTokenValid()
    {
        if (empty($this->password_reset_token)) {
            return false;
        }

        $timestamp = (int) substr($this->password_reset_token, strrpos($this->password_reset_token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * add a Error if passwords doesn't match
     */
    public function validatePassword()
    {
        // method validatePassword generated many different exceptions
        try {
            $match = Yii::$app->security->validatePassword($this->password, $this->password_hash);
        } catch (\Exception $e) {
            $match = false;
        }

        if(!$match)
            $this->addError('password', 100023);

        return $match;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function generatePasswordHash()
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
    }

    /**
     * Generates "remember me" authentication key
     */
    private function generateAuthKey()
    {
        do {
            $auth_key = Yii::$app->getSecurity()->generateRandomString(32);
        } while(self::findIdentityByAccessToken($auth_key) !== null);

        $this->auth_key = $auth_key;
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function scenarios()
    {
        return [
            'save-settings' => ['name', 'surname', 'date_of_birth', 'certificate', 'note', 'language', 'measurement_system', 'starting_dives_number'],
            'login' => ['password'],
            'registration' => ['name', 'surname'],
            'registration-facebook' => []
        ];

    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    //real password, field for creating user model
    public function setPassword($password)
    {
        $this->password = base64_decode($password);
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setFacebookAccessToken($access_token)
    {
        $this->facebookAccessToken = $access_token;
    }

    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $this->generatePasswordHash();
        $this->generateAuthKey();
        $this->registration_type = 'conventional';
        $this->scenario = 'registration';

        if ($this->save())
            return true;

        return false;
    }

    public function registerFacebook()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $this->generateAuthKey();
        $this->registration_type = 'facebook';
        $this->scenario = 'registration-facebook';

        $img = file_get_contents('https://graph.facebook.com/'.$this->facebook_id.'/picture?type=large');
        $filepath = '/tmp/facebook_avatar_'.$this->facebook_id;
        file_put_contents($filepath, $img);
        $this->avatar_file = $filepath;
        $this->uploadAvatar();

        if ($this->save())
            return true;

        $this->addError('registration', 100012);
        return false;
    }

    public function changePassword($password) {
        $this->setPassword($password);
        $this->generatePasswordHash();
        $this->removePasswordResetToken();
        $this->generateAuthKey();
        return $this->save(false);
    }

    public function getDives()
    {
        return $this->hasMany(Dive::className(), ['user_id' => 'id']);
    }

    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['owner_id' => 'id']);
    }

    public function getFollowers()
    {
        return $this->hasMany(User::className(), ['id' => 'follower_id'])
                    ->viaTable('followers', ['target_id' => 'id']);
    }

    // get user's subscriptions to another users
    public function getSubscriptions()
    {
        return $this->hasMany(User::className(), ['id' => 'target_id'])
            ->viaTable('followers', ['follower_id' => 'id']);
    }

    public function fields()
    {
        $fields = [
            'id' => function() {
                return (int)$this->id;
            },
            'avatar' => function() {
                return $this->avatar ? 'www.'.Yii::$app->params['domainName'].'/'.$this->avatar : null;
            },
            'name',
            'surname',
            'facebook_id'
        ];

        if($this->scenario == 'get-settings') {
            $fields['note'] = 'note';
            $fields['date_of_birth'] = 'date_of_birth';
            $fields['certificate'] = 'certificate';
            $fields['measurement_system'] = 'measurement_system';
            $fields['language'] = 'language';
            $fields['starting_dives_number'] = function() {
                return (int)$this->starting_dives_number;
            };
        }

        return $fields;
    }

    public function uploadAvatar() {
        if(!($this->avatar = $this->saveFile($this->avatar_file))) {
            $this->addError('avatar_file', 200037);
            return false;
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }
}
