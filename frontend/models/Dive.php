<?php

namespace frontend\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "dives".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $number
 * @property string $date
 * @property integer $spot_id
 * @property string $conditions
 * @property string $balloons
 * @property string $depth
 * @property string $dive_time
 * @property string $ballast
 * @property integer $temperature_start
 * @property integer $temperature_end
 * @property string $measurement_system
 * @property string $suit
 * @property integer $visibility
 * @property integer $rating
 * @property integer $deleted
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class Dive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dives';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'dateRequired' => ['date', 'required', 'message' => 200006],
            'dateFormat' => ['date', 'date', 'format' => 'yyyy-M-d', 'message' => 200007],
            ['date', 'dateCanChanged'],

            'diveConditionsRequired' => ['conditions', 'required', 'message' => 200008],
            'diveConditionsEnum' => ['conditions', 'in', 'range' => ['standard', 'nightly', 'drift', 'under_ice', 'deep', 'wreck', 'mountain', 'free'], 'message' => 200009],

            'balloonsStuff' => ['balloons', 'balloonsStuff'],

            'depthRequired' => ['depth', 'required', 'message' => 200012],
            'depthNumber' => ['depth', 'number', 'message' => 200013, 'min' => 0.01, 'max' => 9999.99, 'tooSmall' => 200014, 'tooBig' => 2000015],

            'diveTimeRequired' => ['dive_time', 'required', 'message' => 200016],
            'diveTimeTime' => ['dive_time', 'number', 'integerOnly' => true, 'message' => 200017, 'min' => 1, 'max' => 999999999, 'tooSmall' => 200065, 'tooBig' => 200066],

            'ballastNumber' => ['ballast', 'number', 'message' => 200018, 'min' => 0.01, 'max' => 9999.99, 'tooSmall' => 200019, 'tooBig' => 2000020],

            'temperatureStartRequired' => ['temperature_start', 'required', 'message' => 200021],
            'temperatureStartInteger' => ['temperature_start', 'number', 'integerOnly' => true, 'message' => 200022, 'min' => -60, 'max' => 120, 'tooSmall' => 200023, 'tooBig' => 200024],

            'temperatureEndRequired' => ['temperature_end', 'required', 'message' => 200075],
            'temperatureEndInteger' => ['temperature_end', 'number', 'integerOnly' => true, 'message' => 200076, 'min' => -60, 'max' => 120, 'tooSmall' => 200077, 'tooBig' => 200078],

            'measurementSystemRequired' => ['measurement_system', 'required', 'message' => 200025],
            'measurementSystemEnum' => ['measurement_system', 'in', 'range' => ['metric', 'imperial'], 'message' => 200026],

            'suitRequired' => ['suit', 'required', 'message' => 200079],
            'suitEnum' => ['suit', 'in', 'range' => ['dry', 'wet', 'short', 'other'], 'message' => 200080],

            'visibilityRequired' => ['visibility', 'required', 'message' => 200081],
            'visibilityInteger' => ['visibility', 'number', 'integerOnly' => true, 'message' => 200082, 'min' => 0, 'max' => 25, 'tooSmall' => 200083, 'tooBig' => 200084],

            'ratingInteger' => ['rating', 'number', 'integerOnly' => true, 'message' => 200027, 'min' => 1, 'max' => 5, 'tooSmall' => 200028, 'tooBig' => 200029],

            ['note', 'trim'],
            'noteString' => ['note', 'string', 'message' => 200067, 'max' => 65000, 'tooLong' => 200068],

            'spotRequired' => ['spot_id', 'required', 'message' => 200030],
            'spotExist' => ['spot_id', 'spotExist'],
            ['spot_id', 'spotCanChanged'],

            ['partner_id', 'partnerExist'],
            ['partner_id', 'partnerCanChanged'],
            ['partner_id', 'partnerNotConfirmed'],
        ];
    }

    public function balloonsStuff() {
        if($this->balloons === null) return;

        $balloons_info = json_decode($this->balloons, true);
        if($balloons_info === null || count($balloons_info) == 0)
            return $this->addError('balloons', 200011);

        foreach($balloons_info as $balloon_info) {
            if(!(
                isset($balloon_info['material']) &&
                in_array($balloon_info['material'], ['steel','aluminum'], true) &&
                isset($balloon_info['gas']) &&
                (
                    $balloon_info['gas'] ===  'air' ||
                    (
                        $balloon_info['gas'] === 'nitrox' &&
                        isset($balloon_info['gas_percent']) &&
                        is_int($balloon_info['gas_percent']) &&
                        $balloon_info['gas_percent'] >= 21 &&
                        $balloon_info['gas_percent'] <= 100
                    )
                ) &&
                isset($balloon_info['volume']) &&
                in_array($balloon_info['volume'], [10,12,15,18], true) &&
                isset($balloon_info['pressure_start']) &&
                is_int($balloon_info['pressure_start']) &&
                $balloon_info['pressure_start'] >= 10 &&
                $balloon_info['pressure_start'] <= 4351 &&
                isset($balloon_info['pressure_end']) &&
                is_int($balloon_info['pressure_end']) &&
                $balloon_info['pressure_end'] >= 10 &&
                $balloon_info['pressure_end'] <= 4351
            ))
                return $this->addError('balloons', 200011);
        }
    }

    public function spotExist()
    {
        $dive_spot = DiveSpot::find()
                        ->where(['id'=>$this->spot_id])
                        ->andWhere(['or', ['global'=>true, 'deleted'=>false], ['owner_id'=>Yii::$app->getUser()->id, 'deleted'=>false]])
                        ->one();

        if(!isset($dive_spot))
            $this->addError('spot_id', 200063);
    }

    public function partnerExist() {
        $partner = User::find()
                        ->where(['id' => $this->partner_id])
                        ->andWhere(['!=', 'id', $this->user_id])
                        ->one();
        if(!isset($partner))
            $this->addError('partner_id', 200069);
    }

    public function spotCanChanged() {
        if(isset($this->primary_id) && $this->oldAttributes['spot_id'] != $this->spot_id)
            $this->addError('spot_id', 200072);
    }

    public function dateCanChanged() {
        if(isset($this->primary_id) && $this->oldAttributes['date'] != $this->date)
            $this->addError('date_id', 200073);
    }

    public function partnerCanChanged() {
        if(isset($this->primary_id) && $this->oldAttributes['partner_id'] != $this->partner_id)
            $this->addError('partner_id', 200071);
    }

    public function partnerNotConfirmed()
    {
        if(isset($this->id)) {
            $dive = Dive::find()
                ->where(['primary_id' => $this->id])
                ->andWhere(['confirmed' => true])
                ->andWhere(['deleted' => false])
                ->one();

            if (isset($dive))
                $this->addError('partner_id', 200070);
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'spot_id' => 'Place ID',
            'conditions' => 'Dive Conditions',
            'balloons' => 'Balloons',
            'depth' => 'Depth',
            'dive_time' => 'Dive Time',
            'ballast' => 'Ballast',
            'temperature_start' => 'Temperature start',
            'temperature_end' => 'Temperature end',
            'measurement_system' => 'Measurement System',
            'suit' => 'Suit',
            'visibility' => 'Visibility',
            'rating' => 'Rating',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    public function getSpot()
    {
        return $this->hasOne(DiveSpot::className(), ['id' => 'spot_id']);
    }

    public function getImages()
    {
        return $this->hasMany(DivesImage::className(), ['dive_id' => 'id']);
    }

    public function getPartnersDive() {
        $partners_dive = $this->hasOne(Dive::className(), ['id' => 'primary_id'])->where(['deleted' => false])->one();
        if(!$partners_dive)
            $partners_dive = Dive::find()->where(['primary_id' => $this->id, 'deleted' => false])->one();

        return $partners_dive;
    }

    public function fields()
    {
        $fields =  [
            'id' => function() {
                return (int)$this->id;
            },
            'date',
            //change to object in future
            'spot_id' => function() {
                return (int)$this->spot_id;
            },
            'conditions',
            // needs to decode, for prevent double json_encode
            // before send response
            'balloons' => function() {
                return json_decode($this->balloons, true);
            },
            'depth' => function() {
                return round($this->depth, 2);
            },
            'dive_time'=> function() {
                return (int)$this->dive_time;
            },
            'ballast' => function() {
                return round($this->ballast, 2);
            },
            'temperature_start' => function() {
                return (int)$this->temperature_start;
            },
            'temperature_end' => function() {
                return (int)$this->temperature_end;
            },
            'measurement_system',
            'suit',
            'visibility' => function() {
                return (int)$this->visibility;
            },
            'rating' => function() {
                return (int)$this->rating;
            },
            'note',
        ];

        if($this->scenario == 'synchronize') {
            $fields['deleted'] = function () {
                return (bool)$this->deleted;
            };
            $fields['primary'] = function() {
                return !isset($this->primary_id);
            };
            $fields['confirmed'] = function () {
                return (bool)$this->confirmed;
            };
        }


        return $fields;
    }

    public function extraFields()
    {
        return [
            'images' => function() {
                return $this->images;
            },
            'partner' => function() {
                return $this->partner;
            },
        ];
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->created_at = new Expression('NOW()');
        return parent::beforeSave($insert);
    }
}
