<?php

namespace frontend\controllers;

use frontend\models\Post;
use Yii;
use frontend\models\Dive;
use frontend\models\DivesImage;
use yii\base\UnknownPropertyException;
use frontend\models\DiveSpot;
use yii\web\UploadedFile;
use yii\db\Expression;

/**
 * DivesController implements the CRUD actions for Dive model.
 */
class DivesController extends AuthController
{
    private function getSpotId() {
        $spot_info = [
            'country' => Yii::$app->request->post('country'),
            'region' => Yii::$app->request->post('region'),
            'location' => Yii::$app->request->post('location'),
            'spot_name' => Yii::$app->request->post('spot_name'),
            'lat' => Yii::$app->request->post('lat'),
            'lng' => Yii::$app->request->post('lng'),
        ];

        $body_params = Yii::$app->request->getBodyParams();
        unset($body_params['country']);
        unset($body_params['region']);
        unset($body_params['location']);
        unset($body_params['spot_name']);
        unset($body_params['lng']);
        unset($body_params['lat']);
        Yii::$app->request->setBodyParams($body_params);

        if(in_array(!null, $spot_info)) {
            try {
                $spot = new DiveSpot($spot_info + ['owner_id' => Yii::$app->getUser()->id]);
            } catch(UnknownPropertyException $e) {
                return ['errors' => ['general' => [900002]]];
            }
            if (!$spot->validate())
                return ['errors' => $spot->getErrors()];

            $spot->save();
            return $spot->id;
        } else {
            return Yii::$app->request->post('spot_id');
        }
    }

    private function saveImages($dive) {
        $images_save_errors = [];
        foreach(UploadedFile::getInstancesByName('images') as $file) {
            $image = new DivesImage();
            $image->file = $file;

            if($image->upload()) {
                try {
                    $dive->link('images', $image);
                } catch(\Exception $e) {
                    $images_save_errors = array_merge_recursive($images_save_errors, ['file' => [200038]]);
                }
            } else {
                $images_save_errors =  array_merge_recursive($images_save_errors, $image->getErrors());
            }
        }

        if(count($images_save_errors)) {
            $merge_images_save_errors = [];
            foreach ($images_save_errors as $category_errors) {
                $merge_images_save_errors = array_merge($merge_images_save_errors, $category_errors);
            }
            return array_unique($merge_images_save_errors);
        }

        return true;
    }

    private function createPartnersDive($dive) {
        $partners_dive = new Dive([
            'user_id' => Yii::$app->request->post('partner_id') ,
            'date' => $dive->date,
            'spot_id' => $dive->spot_id,
            'partner_id' => $dive->user_id,
            'primary_id' => $dive->id,
            'confirmed' => false
        ]);

        $partners_dive->save(false);
    }

    /**
     * @api {post} /dive Create Dive
     * @apiGroup Dive Actions
     * @apiName create-dive
     * @apiDescription Method will create a new dive, provided that the request contains a valid values. This query can be sending ONLY as multipart/form-data, if you want to send a files. Attention! method will return "success":false, when dive correctly saved, but attached files will has some wrong format or etc.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {date{1902-2037}} date Date of dive. Format: 'yyyy-mm-dd'(e.g.:1999-12-28)
     * @apiParam {string='standard', 'nightly', 'drift', 'under_ice', 'deep', 'wreck', 'mountain', 'free', 'cave', 'training'} conditions Dive conditions
     * @apiParam {Object} balloons Dives balloons(see example), not required
     * @apiParamExample Balloons example {json}
     * [
     *  {
     *      "material":"steel/aluminum/",
     *      "gas":"air"/"nitrox",
     *      "gas_percent":21-100,(only for nitrox!)
     *      "volume":10/12/15/18,
     *      "pressure_start":10-4351,
     *      "pressure_end":10-4351,
     *  },
     *  ...
     * ]
     * @apiParam {number{0.01-9999.99}} depth depth of dive
     * @apiParam {number{1-999999999}}  dive_time  Time of dive in minutes
     * @apiParam {number{0.01-9999.99}} ballast Ballast weight. Not required param
     * @apiParam {number{-60-120}} temperature_start Start temperature of water
     * @apiParam {number{-60-120}} temperature_end End temperature of water
     * @apiParam {string='metric', 'imperial'} measurement_system Name of measurement system
     * @apiParam {string='dry', 'wet', 'short', 'other'} suit Type of suit
     * @apiParam {number{0-25}} visibility Visibility of water
     * @apiParam {rating{1-5}} rating Dives rating. Not required param
     * @apiParam {string{65000}} note Note for dive, not required. All spaces at start and at the end will trimmed
     * @apiParam {files='png','jpeg/jpg','gif'} images Dives images. Must be send as ARRAY, even you want send 1 file.E.g.:
     *      Content-Disposition: form-data; name="images[]"; filename="image.gif"
     *      ...
     *      Content-Disposition: form-data; name="images[]"; filename="image2.gif"
     * @apiParam {number} spot_id id of dive spot, not required, if you pass info about new spot
     * @apiParam {string{3-50}} country country-name(Greece) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} region region-name(Mediterranean Sea) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} location location-name(Cyprus) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} spot_name spot-name(White River) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number{-90-90}} lat latitude of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number{-180-180}} lng longitude of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number} partner_id Id of user, which was partner for this dive. Not required
     *
     * @apiError 200006 the field date is required
     * @apiError 200007 the field date has incorrect format(or to small/big)
     * @apiError 200008 the field conditions is required
     * @apiError 200009 the field conditions has an invalid value
     * @apiError 200011 the field balloons has an invalid value
     * @apiError 200012 the field depth is required
     * @apiError 200013 the field depth must be a number
     * @apiError 200014 the field depth to small
     * @apiError 200015 the field depth to big
     * @apiError 200016 the field dive_time is required
     * @apiError 200017 the field dive_time must be a integer
     * @apiError 200065 the field dive_time to small
     * @apiError 200066 the field dive_time to big
     * @apiError 200018 the field ballast must be a number
     * @apiError 200019 the field ballast to small
     * @apiError 200020 the field ballast to big
     * @apiError 200021 the field temperature_start is required
     * @apiError 200075 the field temperature_end is required
     * @apiError 200022 the field temperature_start must be a integer
     * @apiError 200076 the field temperature_end must be a integer
     * @apiError 200023 the field temperature_start to small
     * @apiError 200077 the field temperature_end to small
     * @apiError 200024 the field temperature_start to big
     * @apiError 200078 the field temperature_end to big
     * @apiError 200025 the field measurement_system is required
     * @apiError 200026 the field measurement_system has an invalid value
     * @apiError 200079 the field suit is required
     * @apiError 200080 the field suit has an invalid value
     * @apiError 200081 the field visibility is required
     * @apiError 200082 the field visibility must be a integer
     * @apiError 200083 the field visibility to small
     * @apiError 200084 the field visibility to big
     * @apiError 200027 the field rating must be a integer
     * @apiError 200028 the field rating to small
     * @apiError 200029 the field rating to big
     * @apiError 200030 the field spot_id is wrong
     * @apiError 200031-200034 if you receive this error - please tell me
     * @apiError 200067 if you receive this error - please tell me
     * @apiError 200068 the note is to big
     * @apiError 200035 the field images isn't files
     * @apiError 200036 the some images has wrong extension
     * @apiError 200037 the some images has wrong MimeType
     * @apiError 200038 you receive this error - please tell me
     * @apiError 200039 the field country is required, if you pass info for NEW dive spot
     * @apiError 200040 the field country must be a string
     * @apiError 200041 the field country too short
     * @apiError 200042 the field country too long
     * @apiError 200043 the field region is required, if you pass info for NEW dive spot
     * @apiError 200044 the field region must be a string
     * @apiError 200045 the field region too short
     * @apiError 200046 the field region too long
     * @apiError 200047 the field location is required, if you pass info for NEW dive spot
     * @apiError 200048 the field location must be a string
     * @apiError 200049 the field location too short
     * @apiError 200050 the field location too long
     * @apiError 200051 the field spot_name is required, if you pass info for NEW dive spot
     * @apiError 200052 the field spot_name must be a string
     * @apiError 200053 the field spot_name too short
     * @apiError 200054 the field spot_name too long
     * @apiError 200055 the field lat is required, if you pass info for NEW dive spot
     * @apiError 200056 the field lat must be a number
     * @apiError 200057 the field lat too small
     * @apiError 200058 the field lat too big
     * @apiError 200059 the field lng is required, if you pass info for NEW dive spot
     * @apiError 200060 the field lng must be a number
     * @apiError 200061 the field lng too small
     * @apiError 200062 the field lng too big
     * @apiError 200063 the spot_id not exist in db
     * @apiError 200069 the partner_id not exist in db
     * @apiError 200074 some error during save file, please tell me
     *
     * @apiSuccess {Object} new dive Object of new dive
     * @apiSuccessExample Success Response Example
     *  {
     *     "dive": {
     *         "id": 35,
     *         "number": 39,
     *         "date": "1997-12-12",
     *         "spot_id": 1,
     *         "conditions": "deep",
     *         "balloons": [
     *             {
     *                 "material": "steel",
     *                 "gas": "nitrox",
     *                 "gas_percent": 33,
     *                 "volume": 15,
     *                 "pressure_start": 60,
     *                 "pressure_end": 55
     *             },
     *             {
     *                 "material": "aluminum",
     *                 "gas": "air",
     *                 "volume": 18,
     *                 "pressure_start": 220,
     *                 "pressure_end": 100
     *             }
     *         ],
     *         "depth": 2,
     *         "dive_time": 87,
     *         "ballast": 4,
     *         "temperature_start": 12,
     *         "temperature_end": 2,
     *         "measurement_system": "metric",
     *         "suit": "dry",
     *         "visibility": 10,
     *         "rating": 2,
     *         "images": [
     *             {
     *                  "id": 20,
     *                  "url": "www.ver2.divenumber.com/dives_images/1/1/590a08ad0269f37bead12e39dfbfebdd.gif"
     *              },
     *              {
     *                  "id": 21,
     *                  "url": "www.ver2.divenumber.com/dives_images/1/1/e6f47f56a764b6e54a34e06cfdd5e4da.png"
     *              }
     *          ],
     *          "partner": {
     *              "id": 3,
     *              "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *          }
     *       },
     *      "success": true
     *  }
     *
     * @apiSampleRequest /dive
     */
    public function actionCreate()
    {
        if(is_array($spot_id = $this->getSpotId()))
            return $spot_id;

        // create new spot
        $data = Yii::$app->request->post();

        // workaround for wrong view
        unset($data['images']);

        $data = [
            'user_id' => Yii::$app->user->getId(),
            'spot_id' => $spot_id
        ] + $data;

        try {
            $dive = new Dive($data);
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }

        if (!$dive->validate())
            return ['errors' => $dive->getErrors()];

        $dive->save();

        // save partners dive, if partner_id is set
        if(Yii::$app->request->post('partner_id') != null)
            $this->createPartnersDive($dive);

        $images_errors = $this->saveImages($dive);
        if(is_array($images_errors)) {
            return ['dive' => $dive->toArray([], ['images', 'partner']), 'errors' => ['images' => array_unique($images_errors)]];
        } else {
            return ['dive' => $dive->toArray([], ['images', 'partner'])];
        }
    }

    /**
     * @api {get} /dives/synchronize/:synchronize_time Dives Synchronize
     * @apiGroup Dive Actions
     * @apiName dives-synchronize
     * @apiDescription Method provide a dives information for new, edited and deleted dives. All useful info contained in object "dives": 1. if 'deleted' is true - you must delete this dive from local DB; 2. if 'id' absent in a local DB - its a new dive; 3. if 'id' present in a local DB - its a edited dive.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{0-999999999}} synchronize_time A last timestamp for synchronize. For first query please use 0
     * @apiSuccess {Object} dives collection Collection of synchronized dives
     * @apiSuccess {number} synchronize_time New synchronize time(timestamp)
     * @apiSuccess {bool} fully Maximum limit of a dives for the request - 50 dives in response. If param is FALSE, then you need send a new synchronize request, with a new synchronize_time
     *
     * @apiSuccessExample Success Response Example
     * {
     * "dives": [
     *  {
     *      "id": 35,
     *      "number": 39,
     *      "date": "1997/12/12",
     *      "spot_id": 1,
     *      "conditions": "deep",
     *      "balloons": [
     *          {
     *              "material": "steel",
     *              "gas": "nitrox",
     *              "gas_percent": 33,
     *              "volume": 15,
     *              "pressure_start": 60,
     *              "pressure_end": 55
     *          },
     *          {
     *              "material": "aluminum",
     *              "gas": "air",
     *              "volume": 18,
     *              "pressure_start": 220,
     *              "pressure_end": 100
     *          }
     *      ],
     *      "depth": 2,
     *      "dive_time": "12:12:12",
     *      "ballast": 4,
     *      "temperature_Start": 12,
     *      "temperature_end": 2,
     *      "measurement_system": "imperial",
     *      "suit": "dry",
     *      "visibility": 10,
     *      "rating": 2,
     *      "images": [
     *          {
     *              "id": 20,
     *              "url": "www.ver2.divenumber.com/dives_images/1/1/590a08ad0269f37bead12e39dfbfebdd.gif"
     *          },
     *          {
     *              "id": 21,
     *              "url": "www.ver2.divenumber.com/dives_images/1/1/e6f47f56a764b6e54a34e06cfdd5e4da.png"
     *          }
     *      ],
     *      "note": "sdfsdfsdf",
     *      "deleted": false,
     *      "partner": {
     *          "id": 3,
     *          "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *       }
     *      "primary": true, // true - if, user created this dive, false - if this dive was created by partner
     *      "confirmed" true, // true - default, or if this dive was created by partner and user accepted, that this dive is correct
     *  },
     * ],
     * "fully": true,
     * "synchronize_time": 1455490694,
     * "success": true
     * }
     * @apiSampleRequest /dives/synchronize/:synchronize_time
     */
    public function actionSynchronize($synchronize_time) {
        $dives = Yii::$app->user->identity->getDives()
            ->with('images')
            ->andWhere(['>', 'updated_at', new Expression("FROM_UNIXTIME($synchronize_time)")])
            ->orderBy('updated_at')
            ->limit(50)
            ->all();

        if(count($dives) > 0)
            $synchronize_time = (new \DateTime(end($dives)->updated_at))->getTimestamp();

        array_walk($dives, function(&$dive) {
            $dive->scenario = 'synchronize';
            $dive = $dive->toArray([], ['images', 'partner']);
        });

        return [
            'dives' => $dives,
            'fully' => count($dives) === 50 ? false : true,
            'synchronize_time' => (int)$synchronize_time
        ];
    }

    /**
     * @api {delete} /dive/:id Delete dive
     * @apiGroup Dive Actions
     * @apiName delete-dive
     * @apiDescription Method deletes a dive.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{1-999999999}} id A dive id
     * @apiError 200064 the id not exist in db / not belongs to user
     *
     * @apiSampleRequest /dive/:id
     */
    public function actionDelete($id) {
        $dive = Yii::$app->user->identity->getDives()->where(['id'=>$id, 'deleted'=>false])->one();
        if(isset($dive)) {
            // if exist partner's dive - clear field partner_id
            if(($partners_dive = $dive->getPartnersDive())) {
                $partners_dive->partner_id = null;
                $partners_dive->primary_id = null;
                $partners_dive->save();
            }

            $dive->deleted = true;
            $dive->save();

            $post = Post::findOne(['dive_id' => $id]);
            if($post)
                $post->delete();
        } else
            return ['errors' => ['dive' => [200064]]];
    }

    /**
     * @api {post} /dive Update Dive
     * @apiGroup Dive Actions
     * @apiName update-dive
     * @apiDescription Method will update a users dive, provided that the request contains a valid values. This query can be sending ONLY as multipart/form-data, if you want to send a files. Attention! method will return "success":false, when dive correctly saved, but attached files will has some wrong format or etc. All request parameters aren't required.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{1-999999999}} id A dive id
     * @apiParam {number{1-999999999}} number Number of dive
     * @apiParam {date{1902-2037}} date Date of dive. Format: 'yyyy-mm-dd'(e.g.:1999-12-28)
     * @apiParam {string='standard', 'nightly', 'drift', 'under_ice', 'deep', 'wreck', 'mountain', 'free'} conditions Dive conditions
     * @apiParam {Object} balloons Dives balloons(see example in 'Dive Create').
     * @apiParam {number{0.01-9999.99}} depth depth of dive
     * @apiParam {number{1-999999999}}  dive_time  Time of dive in minutes
     * @apiParam {number{0.01-9999.99}} ballast Ballast weight. Not required param
     * @apiParam {number{-60-120}} temperature_start Start temperature of water
     * @apiParam {number{-60-120}} temperature_end End temperature of water
     * @apiParam {string='metric', 'imperial'} measurement_system Name of measurement system
     * @apiParam {string='dry', 'wet', 'short', 'other'} suit Type of suit
     * @apiParam {number{0-25}} visibility Visibility of water
     * @apiParam {rating{1-5}} rating Dives rating. Not required param
     * @apiParam {string{65000}} note Note for dive, not required. All spaces at start and at the end will trimmed
     * @apiParam {Object} deleted_images Deleted images id's (see example)
     * @apiParamExample Deleted images example {json}
     * [3,6,7]
     * @apiParam {files='png','jpeg/jpg','gif'} images Dives images. Must be send as ARRAY, even you want send 1 file.:
     * @apiParam {number} spot_id id of dive spot, not required, if you pass info about new spot
     * @apiParam {string{3-50}} country country-name(Greece) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} region region-name(Mediterranean Sea) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} location location-name(Cyprus) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {string{3-50}} spot_name spot-name(White River) of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number{-90-90}} lat latitude of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number{-180-180}} lng longitude of NEW dive spot, not required, if you pass spot_id
     * @apiParam {number} partner_id Id of user, which was partner for this dive. Not required. If partner was confirm already - will be error, because you cann't change partner, if partner was confirmed this dive
     *
     * @apiError 200070 partner was confirmed dive, you cann't change partner
     * @apiError 200071 it's dive was created as a partner's dive, you cann't change a partner
     * @apiError 200072 it's dive was created as a partner's dive, you cann't change a spot
     * @apiError 200073 it's dive was created as a partner's dive, you cann't change a date
     *
     * @apiSuccess {Object} new dive Object of updated dive(see example in 'Dive Create')
     *
     * @apiSampleRequest /dive/:id
     */
    public function actionUpdate($id) {
        $dive = Yii::$app->user->identity->getDives()->where(['id'=>$id, 'deleted' => false])->one();
        if(isset($dive)) {
            // workaround
            //$data = array_filter(Yii::$app->request->post());
            if(is_array($data['spot_id'] = $this->getSpotId()))
                return $data['spot_id'] ;
            if(isset($data['deleted_images'])) {
                $images = $dive->getImages()->where(['in', 'id', json_decode($data['deleted_images'], true)])->all();
                foreach($images as $image) {
                    $image->delete();
                }
                unset($data['deleted_images']);
            }

            $dive->setAttributes(array_filter($data));

            if (!$dive->validate())
                return ['errors' => $dive->getErrors()];

            // if it dive which was created as partners dive - check if it confirmed
            if(isset($dive->primary_id)) {
                if(!$dive->confirmed)
                    $dive->confirmed = true;
            } else {
                // if partner changing - delete old dive if it possible and create new dive
                if(
                    Yii::$app->request->post('partner_id') != null &&
                    $dive->oldAttributes['partner_id'] != $dive->partner_id
                ) {
                    // we delete this dive for create correct 'created_at' etc. fields
                    // for new partners dive and therefore, correct working request for synchronize
                    if(isset($dive->oldAttributes['partner_id']))
                        Dive::find()->where(['primary_id' => $dive->id])->one()->delete();

                    $this->createPartnersDive($dive);
                }
            }

            $dive->save();

            $images_errors = $this->saveImages($dive);
            if(is_array($images_errors)) {
                return ['dive' => $dive->toArray([], ['images', 'partner']), 'errors' => ['images' => array_unique($images_errors)]];
            } else {
                return ['dive' => $dive->toArray([], ['images', 'partner'])];
            }

        } else
            return ['errors' => ['dive' => [200064]]];
    }

    /**
     * @api {post} /dive/share/:id Share dive
     * @apiGroup Wall Actions
     * @apiName share-dive
     * @apiDescription Method shares a dive.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{1-999999999}} id A dive id
     *
     * @apiError 300001 this dive was shared early
     *
     * @apiSampleRequest /dive/share/:id
     */
    public function actionShare($id) {
        $dive = Yii::$app->user->identity->getDives()->where(['id'=>$id, 'deleted'=>false])->one();
        if(isset($dive)) {
            $post = new Post();
            $post->owner_id = Yii::$app->user->identity->id;
            $post->dive_id = $id;
            if($post->validate())
                $post->save();
            else
                return ['errors' => $post->getErrors()];
        } else
            return ['errors' => ['dive' => [200064]]];
    }
}
