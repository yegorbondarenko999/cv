<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Like;

/**
 * CommentsController implements the CRUD actions for Dive model.
 */
class LikesController extends AuthController
{
    /**
     * @api {post} /like Like a post
     * @apiGroup Wall Actions
     * @apiName like-post
     * @apiDescription Method set a like for post.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} post_id id of post
     *
     * @apiError 300015 this post was liked already
     *
     * @apiSampleRequest /like
     */
    public function actionCreate()
    {
        $like = new Like(Yii::$app->request->post());
        $like->author_id = Yii::$app->user->identity->id;
        if(!($like->validate() && $like->save()))
            return ['errors' => $like->getErrors()];
    }

    /**
     * @api {delete} /post/:post_id/like Delete like
     * @apiGroup Wall Actions
     * @apiName delete-like
     * @apiDescription Method will deleted like
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} post_id Post id
     *
     * @apiError 300016 the post_id not exist in db / user wasn't put like early
     *
     * @apiSampleRequest /post/:post_id/like
     */
    public function actionDelete($post_id) {
        $like = Like::find()
            ->where(['post_id'=>$post_id])
            ->andWhere(['author_id' => Yii::$app->getUser()->id])
            ->one();

        if(isset($like))
            $like->delete();
        else
            return ['errors' => ['comment' => [300016]]];
    }
}