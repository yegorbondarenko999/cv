<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Post;
use frontend\models\PostsImage;
use frontend\models\User;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 * PostsController implements the CRUD actions for Dive model.
 */
class PostsController extends AuthController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                ],
            ]
        ] + parent::behaviors();
    }

    private function saveImages($post) {
        $images_save_errors = [];
        foreach(UploadedFile::getInstancesByName('images') as $file) {
            $image = new PostsImage();
            $image->file = $file;

            if($image->upload()) {
                try {
                    $post->link('images', $image);
                } catch(\Exception $e) {
                    $images_save_errors = array_merge_recursive($images_save_errors, ['file' => [200038]]);
                }
            } else {
                $images_save_errors =  array_merge_recursive($images_save_errors, $image->getErrors());
            }
        }

        if(count($images_save_errors)) {
            $merge_images_save_errors = [];
            foreach ($images_save_errors as $category_errors) {
                $merge_images_save_errors = array_merge($merge_images_save_errors, $category_errors);
            }
            return array_unique($merge_images_save_errors);
        }

        return true;
    }

    /**
     * @api {get} /posts/:user_id?time-offset=:time_offset Get wall
     * @apiGroup Wall Actions
     * @apiName obtain-wall
     * @apiDescription Method receive users posts(only old posts!! for "pull-to-refresh" request please use this request without parameter time_offset)
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} user_id User id
     * @apiParam {number} time_offset Upper boundary of time, from which searching posts
     *
     * @apiSuccess {Object} posts Collection of posts
     * @apiSuccess {number} time_offset New offset time(timestamp)
     * @apiSuccess {bool} fully Maximum limit of posts for the request - 50 posts in response. If param is FALSE, then you need send a new synchronize request, with a new time_offset
     * @apiSuccessExample Success Response Example
     * {
     *  "posts": [
     *      {
     *          "id": 9,
     *          "note": "22df df gasd asasdfa sdf dasf das fsdafs df5",
     *          "created_at": "2016-03-22 23:47:21",
     *          "image": "www.ver2.divenumber.com/dives_images/1/1/6b920e4f0828e1e6594876b9620c584c.jpg",
     *          "liked": true,
     *      },
     *      ...
     *  ],
     *  "time_offset": 2396492364,
     *  "fully": true,
     *  "success": true
     * }
     *
     * @apiError 300026 user with 'user_id' isn't exist
     *
     * @apiSampleRequest /posts/:user_id?time-offset=:time_offset
     */

    /**
     * @api {get} /posts?spot-id=:spot_id&time-offset=:time_offset Get posts for spot
     * @apiGroup Wall Actions
     * @apiName obtain-spots-posts
     * @apiDescription Method receive spots' posts(only old posts!! for "pull-to-refresh" request please use this request without parameter time_offset)
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} spot_id Spot id
     * @apiParam {number} time_offset Upper boundary of time, from which searching posts
     *
     * @apiSuccess {Object} posts Collection of posts
     * @apiSuccess {number} time_offset New offset time(timestamp)
     * @apiSuccess {bool} fully Maximum limit of posts for the request - 50 posts in response. If param is FALSE, then you need send a new synchronize request, with a new time_offset
     * @apiSuccessExample Success Response Example
     * {
     *  "posts": [
     *      {
     *          "id": 9,
     *          "note": "22df df gasd asasdfa sdf dasf das fsdafs df5",
     *          "created_at": "2016-03-22 23:47:21",
     *          "image": "www.ver2.divenumber.com/dives_images/1/1/6b920e4f0828e1e6594876b9620c584c.jpg",
     *          "liked": true,
     *      },
     *      ...
     *  ],
     *  "time_offset": 2396492364,
     *  "fully": true,
     *  "success": true
     * }
     *
     * @apiSampleRequest /posts?spot-id=:spot_id&time-offset=:time_offset
     */

    public function actionIndex($user_id = null) {
        $posts = (new Query);

        if(isset($user_id)) {
            if (is_null(User::findOne($user_id)))
                return ['errors' => ['general' => [300026]]];
            $posts->andWhere(['posts.owner_id'=>$user_id]);
        }

        $time_offset = (int)Yii::$app->request->get('time-offset');
        if($time_offset > 0)
            $posts->andWhere(['<', 'posts.created_at', new Expression("FROM_UNIXTIME($time_offset)")]);

        $spot_id = (int)Yii::$app->request->get('spot-id');
        if($spot_id > 0)
            $posts->andWhere(['dives.spot_id'=>$spot_id]);

        $posts->select([
                'id' => 'posts.id',
                'note' => 'CONCAT_WS(
                        " ",
                        REPLACE(dives.date, "-", "."),
                        CONCAT_WS(
                            "/",
                            dive_spots.country,
                            dive_spots.region,
                            dive_spots.location,
                            dive_spots.spot_name
                        ),
                        CONCAT(dives.depth, "M"),
                        CONCAT(dives.dive_time, "min"),
                        CONCAT(
                            dives.temperature_start,
                            CASE WHEN dives.measurement_system = "metric" THEN "°C" ELSE "°F" END,
                            "/",
                            dives.temperature_end,
                            CASE WHEN dives.measurement_system = "metric" THEN "°C" ELSE "°F" END
                        )
                    )'
                ,
                'image' => 'CONCAT("www.", "'.Yii::$app->params['domainName'].'", "/", CASE WHEN dives_images.url IS NOT NULL THEN dives_images.url ELSE posts_images.url END)',
                // not use CASE NULL THEN FALSE ELSE TRUE, because bool will return as 0/1,
                // and then we must replace 0=>false, 1=>true in php
                'liked' => new Expression('likes.id'),
                'created_at' => 'posts.created_at'
            ])
            ->from('posts')
            ->leftJoin('dives', ['posts.dive_id' => new Expression('dives.id')])
            ->leftJoin('dive_spots', ['dives.spot_id' => new Expression('dive_spots.id')])
            ->leftJoin('dives_images', ['dives_images.dive_id' => new Expression('dives.id')])
            ->leftJoin('posts_images', ['posts_images.post_id' => new Expression('posts.id')])
            ->leftJoin('likes', [
                'likes.post_id' => new Expression('posts.id'),
                'likes.author_id' => Yii::$app->user->identity->id
            ])
            // get first image
            ->groupBy(['posts.id'])
            ->orderBy('posts.id DESC')
            ->limit(50);

        $posts = $posts->all();

        array_walk($posts, function(&$v) {
            $v['liked'] = isset($v['liked']) ? true : false;
        });

        $new_time_offset = null;
        if(count($posts))
            $new_time_offset = strtotime($posts[count($posts)-1]['created_at']);

        $fully = count($posts) == 50 ? false : true;

        return [
            'posts' => $posts,
            'time_offset' => $new_time_offset,
            'fully' => $fully,
        ];
    }

    /**
     * @api {post} /post Create post
     * @apiGroup Wall Actions
     * @apiName create-post
     * @apiDescription Method creates a post.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {string{1-65000}} note note for the post. All spaces at start and at the end will trimmed
     * @apiParam {files='png','jpeg/jpg','gif'} images Dives images. Must be send as ARRAY, even you want send 1 file.E.g.:
     *  Content-Disposition: form-data; name="images[]"; filename="image.gif"
     *  ...
     *  Content-Disposition: form-data; name="images[]"; filename="image2.gif"
     *
     * @apiSuccessExample Success Response Example
     * {
     *  "post": {
     *      "id": 9,
     *      "note": "22df df gasd asasdfa sdf dasf das fsdafs df5",
     *      "image": "url",
     *      "created_at": "2016-03-22 23:47:21"
     *  },
     *  "success": true
     * }
     *
     * @apiError 300004 note is required
     * @apiError 300002 note is too short
     * @apiError 300003 note is long
     * @apiError 300017-300020 if you receive this error - please tell me
     * @apiError 300021 the field images isn't files
     * @apiError 300022 the some images has wrong extension
     * @apiError 300023 the some images has wrong MimeType
     * @apiError 300024 some error during save file, please tell me
     * @apiError 300025 you cann't create post without image
     *
     * @apiSampleRequest /post
     */
    public function actionCreate()
    {
        $post = new Post();
        $post->owner_id = Yii::$app->user->identity->id;
        $post->note = Yii::$app->request->post('note');

        if($post->validate()) {
            $post->save();

            $images_errors = $this->saveImages($post);

            if(!count($post->images)) {
                $images_errors = is_bool($images_errors) ? [] : $images_errors;
                array_push($images_errors, 300025);
            }

            if(is_array($images_errors)) {
                $post->delete();
                return ['errors' => ['images' => array_unique($images_errors)]];
            } else {
                return ['post' => Post::findOne($post->id)->toArray([], ['image'])];
            }
        } else
            return ['errors' => $post->getErrors()];
    }

    /**
     * @api {delete} /post/:id Delete post
     * @apiGroup Wall Actions
     * @apiName delete-post
     * @apiDescription Method has deleted post
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} id Post id
     *
     * @apiError 300005 the id not exist in db / not belongs to user
     *
     * @apiSampleRequest /post/:id
     */
    public function actionDelete($id) {
        $post = Yii::$app->user->identity->getPosts()->where(['id'=>$id])->one();
        if(isset($post)) {
            $post->delete();
        } else
            return ['errors' => ['dive' => [300005]]];
    }
}