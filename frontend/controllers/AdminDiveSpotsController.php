<?php
namespace frontend\controllers;

use frontend\models\Dive;
use Yii;
use frontend\models\DiveSpot;
use yii\data\Pagination;
use yii\filters\AccessControl;

class AdminDiveSpotsController extends SiteBaseController
{

    public function behaviors() {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $action = Yii::$app->controller->action->id;
                            $controller = Yii::$app->controller->id;
                            $route = "$controller/$action";
                            if (Yii::$app->user->can($route)) {
                                return true;
                            }
                        },
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionUnmoderatedList() {

        $spots = DiveSpot::find()->where(['moderated'=>false]);
        $pages = new Pagination([
            'totalCount' => $spots->count(),
            'defaultPageSize' => 10
        ]);
        $spots = $spots->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('unmoderated-list', [
            'spots' => $spots,
            'pages' => $pages,
        ]);
    }

    public function actionUnmoderatedAccept($id) {
        $original_id = Yii::$app->request->post('original-id') ? Yii::$app->request->post('original-id') : null;
        if(isset($original_id)) {
            $original_spot = DiveSpot::find()->where([
                'id' => (int)$original_id,
                'moderated' => true
            ])->one();
            if(!$original_spot)
                return ['errors' => ['general' => [200030]]];

            Dive::updateAll([
                'spot_id' => $original_spot->id
            ], [
                'spot_id' => $id
            ]);

            $spot = DiveSpot::findOne($id);
            $spot->moderated = $spot->deleted = true;
            $spot->save();
            return true;
        } else {
            $spot = DiveSpot::findOne($id);
            $spot->moderated = true;

            if($spot->load(Yii::$app->request->post()) && $spot->validate()) {
                if($spot->global)
                    $spot->owner_id = null;
                $spot->save();
                return true;
            } else
                return ['errors' => $spot->getErrors()];
        }
    }

    public function actionIndex() {
        $filters = Yii::$app->request->get('filter');

        if(strlen($filters)) {
            $filters = array_map(function ($v) {
                return trim($v);
            },
                array_values(array_filter(explode(',', $filters), function ($v) {
                    return strlen(trim($v));
                })));
        } else
            $filters = [];

        if(count($filters)) {
            $spots = DiveSpot::find()
                ->select([
                    'id' => 'id',
                    'value' => "CONCAT_WS(', ',country,region,location,spot_name,lat,lng)",
                ])
                ->where(['moderated'=>true, 'global'=>true])
                ->andWhere(['like', "CONCAT_WS(', ',country,region,location,spot_name,lat,lng)", $filters])
                ->limit(20)
                ->asArray()
                ->all();

            return ['spots' => $spots];
        } else {
            return [];
        }
    }
}