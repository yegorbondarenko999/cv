<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;


class ApiController extends Controller {

    //необходимо для того, чтобы можно было отправлять данные не тоолько с сайта но и еще с устройств
    public $enableCsrfValidation = false;

    public function afterAction($action, $result)
    {
        if(!is_array($result))
            $result = [];

        if(!array_key_exists('errors', $result))
            $result['success'] = true;
        else
            $result['success'] = false;

        if($result['success'] === false && isset($result['errors'])) {
            $errors_numbers = [];
            foreach($result['errors'] as $type => $values) {
                array_walk($values, function(&$v) {$v = (int)$v;});
                $errors_numbers = array_merge($errors_numbers, $values);
            }
            $result['errors'] = array_values(array_unique($errors_numbers, SORT_NUMERIC));
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::afterAction($action, $result);
    }
}