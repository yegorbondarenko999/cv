<?php
namespace frontend\controllers;

use Yii;
use frontend\models\User;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookAuthenticationException;
use frontend\models\LoginForm;
use frontend\models\RegisterForm;
use frontend\models\LoginFacebookForm;
use frontend\models\RegisterFacebookForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\base\UnknownPropertyException;

class LoginController extends ApiController {


    /**
     * @api {post} /register User Registration
     * @apiGroup Login Actions
     * @apiName register
     * @apiDescription Method will create a new user, provided that the request contains a valid values
     * @apiParam {string{5..100}} email contains the e-mail.  All spaces at start and at the end will trimmed
     * @apiParam {string(base64){6..100}} password contains the password.
     * @apiParam {string{1-50}} name User's name. All spaces at start and at the end will trimmed
     * @apiParam {string{1-50}} surname User's surname. All spaces at start and at the end will trimmed
     *
     * @apiSuccess {string} auth_key contains the unique key for user authentication
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": true,
     *       "auth_key": "7g76shhjk43_34534"
     *     }
     *
     * @apiError 100001 the field email is required
     * @apiError 100002 the field email doesn't like a e-mail address
     * @apiError 100003 the field password must be send in a string format
     * @apiError 100004 the field email is too short
     * @apiError 100005 the field email is too long
     * @apiError 100006 this email is already taken
     * @apiError 100007 the field password is required
     * @apiError 100008 the field password must be send in a string format
     * @apiError 100009 the field password is too short. Attention! This error may be caused by situation, when password doesn't encoded in base64, but encode function tries decode string and it decoded as some string trash, like ���
     * @apiError 100010 the field password is too long
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success" : false,
     *       "errors" : [1,2,3]
     *     }
     * @apiSampleRequest /register
     */
    public function actionRegister()
    {
        try {
            $form = new RegisterForm([
                'email' => Yii::$app->request->post('email'),
                'password' => Yii::$app->request->post('password'),
            ]);
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }

        if (!$form->validate())
            return ['errors' => $form->getErrors()];

        $user = new User([
                'login' => trim(Yii::$app->request->post('email')),
                'password' => Yii::$app->request->post('password'),
                'name' => trim(Yii::$app->request->post('name')),
                'surname' => trim(Yii::$app->request->post('surname')),
            ]);

        if($user->register() === true)
            return ['auth_key' => $user->auth_key];
        return ['errors' => $user->getErrors()];
    }

 /**
  * @api {post} /sign-in Sign-in
  * @apiGroup Login Actions
  * @apiName sign-in
  * @apiDescription Method will login a user, provided that the request contains a valid values
  * @apiParam {string{5..100}} email contains the e-mail
  * @apiParam {string(base64){6..100}} password contains the password.
  * @apiSuccess {string} auth_key contains the unique key for user authentication
  *
  * @apiError 100023 email and/or password are/is wrong
  * 
  * @apiSampleRequest /sign-in
  */
    public function actionLogin()
    {
        try {
            $form = new LoginForm(Yii::$app->request->post());
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }
        if (!$form->validate())
            return ['errors' => $form->getErrors()];


        $user = User::findOne(['login' => trim(Yii::$app->request->post('email')), 'registration_type' => 'conventional']);
        $user->setPassword(Yii::$app->request->post('password'));
        $user->scenario = 'login';

        if($user->validate())
            return ['auth_key' => $user->auth_key];
        else
            return ['errors' => $user->getErrors()];
    }

    private function actionRegisterFacebook($facebook_user)
    {
        $form = new RegisterFacebookForm([
            'login' => $facebook_user->getId(),
        ]);
        if (!$form->validate())
            return ['errors' => $form->getErrors()];

        $user = new User([
            'login' => $facebook_user->getId(),
            'facebook_id' => $facebook_user->getId(),
            'name' => $facebook_user->getFirstName(),
            'surname' => $facebook_user->getLastName(),
        ]);

        if($user->registerFacebook($facebook_user) === true)
            return ['auth_key' => $user->auth_key];
        return ['errors' => $user->getErrors()];
    }

    /**
     * @api {post} /sign-in-facebook Sign-in via facebook
     * @apiGroup Login Actions
     * @apiName sign-in-facebook
     * @apiDescription Method will login or create a new user, provided that the request contains a valid values
     * @apiParam {string} access_token contains special access token received from facebook. More information(search access_tocken): <a href="https://developers.facebook.com/docs/facebook-login/access-tokens/expiration-and-extension">https://developers.facebook.com/docs/facebook-login/access-tokens/expiration-and-extension</a>
     * @apiSuccess {string} auth_key contains the unique key for user authentication
     *
     * @apiError 100021 the field access_token is required
     * @apiError 100018 receive some error from facebook server
     * @apiError 100019 receive some error from facebook application
     * @apiError 100020 receive some facebook authentication error
     * @apiError 100022 receive undefined facebook error
     *
     * @apiError 100013-100017 if you receive this error - please tell me (some error in server-side registration via facebook)
     *
     * @apiSampleRequest /sign-in-facebook
     */
    public function actionLoginFacebook()
    {
        try {
            $form = new LoginFacebookForm(Yii::$app->request->post());
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }
        if (!$form->validate())
            return ['errors' => $form->getErrors()];

        $fb = new Facebook(Yii::$app->params['facebook']);

        try {
            $facebook_user = $fb->get(
                '/me?fields=id,first_name,last_name',
                Yii::$app->request->post('access_token')
            )->getGraphUser();
        } catch(FacebookResponseException $e) {
            return ['errors' => ['facebook'=>[100018]]];
        } catch(FacebookAuthenticationException $e) {
            return ['errors' => ['facebook'=>[100020]]];
        } catch(FacebookSDKException $e) {
            return ['errors' => ['facebook'=>[100019]]];
        } catch(\Exception $e) {
            return ['errors' => ['facebook'=>[100022]]];
        }

        $user = User::findOne(['login' => trim($facebook_user->getId()), 'registration_type' => 'facebook']);

        if($user === null)
            return $this->actionRegisterFacebook($facebook_user);

        return ['auth_key' => $user->auth_key];
    }

    /**
     * @api {post} /request-password-reset Request for reset password via email
     * @apiGroup Login Actions
     * @apiName request-password-reset
     * @apiDescription Method will send an email with password-reset-code, by which a user will can change password
     * @apiParam {string{5..100}} email contains the e-mail
     *
     * @apiError 100024 email can not be send now.
     * @apiError 100025 email is not belong to anyone user.
     *
     * @apiSampleRequest /request-password-reset
     */
    public function actionRequestPasswordReset()
    {
        try {
            $form = new PasswordResetRequestForm(Yii::$app->request->post());
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }

        if ($form->validate()) {
            if (!$form->sendEmail())
                return ['errors' => ['email' => 100024]];
        } else
            return ['errors' => $form->getErrors()];
    }

    /**
     * @api {post} /reset-password Reset password by reset-token
     * @apiGroup Login Actions
     * @apiName reset-password
     * @apiDescription Method will set a new password for a user
     * @apiParam {string{5..100}} reset_token contains the reset-token(a code from email)
     * @apiParam {string(base64){6..100}} password contains the new password.
     * @apiError 100027 the field reset_token is required
     * @apiError 100026 the field reset_token isn't valid or old as the hills
     *
     * @apiSampleRequest /reset-password
     */
    public function actionResetPassword()
    {
        try {
            $form = new ResetPasswordForm(Yii::$app->request->post());
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }

        if ($form->validate()) {
            $user = User::findOne(['password_reset_token' => trim(Yii::$app->request->post('reset_token')), 'registration_type' => 'conventional']);
            $user->changePassword(Yii::$app->request->post('password'));
        } else
            return ['errors' => $form->getErrors()];
    }
}