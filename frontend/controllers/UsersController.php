<?php

namespace frontend\controllers;

use Yii;
use yii\web\UploadedFile;
use frontend\models\User;
use yii\db\Query;
use yii\db\Expression;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookAuthenticationException;
use frontend\models\LoginFacebookForm;
use yii\base\UnknownPropertyException;

class UsersController extends AuthController {

    /**
     * @api {post} /user/settings Save user's settings
     * @apiGroup User Actions
     * @apiName user-save-settings
     * @apiDescription Method will save user's settings. This query can be sending ONLY as multipart/form-data, if you want to send a files.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {string{1-50}} name User's name. All spaces at start and at the end will trimmed
     * @apiParam {string{1-50}} surname User's surname. All spaces at start and at the end will trimmed
     * @apiParam {date{1902-2037}} date_of_birth Date of birth. Format: 'yyyy-mm-dd'(e.g.:1999-12-28)
     * @apiParam {string{10-30}} certificate User's certificate. All spaces at start and at the end will trimmed
     * @apiParam {string{65000}} note User's note. All spaces at start and at the end will trimmed
     * @apiParam {string='metric', 'imperial'} measurement_system Name of measurement system
     * @apiParam {string='eng', 'rus', 'ukr'} language App language. Default 'eng'
     * @apiParam {number{1-999999999}} starting_dives_number Start number for dives. Need for client's app. Default 1
     * @apiParam {file='png','jpeg/jpg','gif'} avatar User's avatar. E.g.:
     *      Content-Disposition: form-data; name="avatar"; filename="image.gif"
     * @apiError 400001 the field name is required
     * @apiError 400002 the field name must be a string
     * @apiError 400003 the field name is too short
     * @apiError 400004 the field name is too long
     * @apiError 400011 the field surname is required
     * @apiError 400012 the field surname must be a string
     * @apiError 400013 the field surname is too short
     * @apiError 400014 the field surname is too long
     * @apiError 400005 the field date_of_birth has a wrong format
     * @apiError 400006 the field certificate must be a string
     * @apiError 400007 the field certificate is too short
     * @apiError 400008 the field certificate is too long
     * @apiError 400009 the field note must be a string
     * @apiError 400010 the field note is too long
     * @apiError 400015 the field language is required
     * @apiError 400016 the field language has an invalid value
     * @apiError 400017 the field measurement_system is required
     * @apiError 400018 the field measurement_system has an invalid value
     * @apiError 400019 the field starting_dives_number is required
     * @apiError 400020 the field starting_dives_number must be a integer
     * @apiError 400021 the field starting_dives_number to small
     * @apiError 400022 the field starting_dives_number to big
     *
     * @apiSuccess {Object} new info Object with user's info
     * @apiSuccessExample Success Response Example
     *  {
     *     "user": {
     *         "id": 35,
     *         "avatar": "www.ver2.divenumber.com/dives_images/1/1/590a08ad0269f37bead12e39dfbfebdd.gif",
     *         "name": "q",
     *         "surname": "q"
     *       },
     *      "success": true
     *  }
     *
     * @apiSampleRequest /user/settings
     */
    public function actionSaveSettings() {
        $user = Yii::$app->user->identity;
        $user->scenario = 'save-settings';

        $user->setAttributes(array_map(function($v) { return trim($v);}, Yii::$app->request->post()));

        if ($user->validate()) {
            if($avatar_info = UploadedFile::getInstanceByName('avatar')) {
                $user->avatar_file = $avatar_info->tempName;
                $user->uploadAvatar();
            }
            $user->save();
            return ['user' => $user];
        } else
            return ['errors' => $user->getErrors()];
    }

    /**
     * @api {get} /user/settings Get user's settings
     * @apiGroup User Actions
     * @apiName user-get-settings
     * @apiDescription Method will receive user's settings.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     * @apiSuccessExample Success Response Example
     *  {
     *     "user": {
     *         "id": 35,
     *         "avatar": "www.ver2.divenumber.com/dives_images/1/1/590a08ad0269f37bead12e39dfbfebdd.gif",
     *         "name": "q",
     *         "surname": "q"
     *         "note": "asdasdasd",
     *         "certificate": "adasd",
     *         "date_of_birth": "1997-12-12"
     *       },
     *      "success": true
     *  }
     *
     * @apiSampleRequest /user/settings
     */
    public function actionGetSettings() {
        $user = Yii::$app->user->identity;
        $user->scenario = 'get-settings';
        if ($user) {
            return ['user' => $user];
        } else
            return ['errors' => ['general' => [900001]]];
    }

    /**
     * @api {get} /users?username=:username&offset=:offset Search user's
     * @apiGroup User Actions
     * @apiName users-search
     * @apiDescription Method provide a users' information which consist users input. Attention!!! Method receive information about max 100 users. For receive info more then 100 users, please use field 'offset'. Attention 2!! You can get info about users which appropriate by username EXCEPT user, which send this request
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     * @apiParam {text} username Username filter
     * @apiParam {number} offset Offset of users(when, you receive info fot first 100 users, then you must pass param 'offset' = 100 in next request)
     *
     * @apiSuccess {Object} users collection Collection of filtered users
     *
     * @apiSuccessExample Success Response Example
     *  {
     *      "users": [
     *          {
     *              "id": 3,
     *              "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *          }
     *      ],
     *      "success": true
     *  }
     *
     * @apiSampleRequest /users?username=:username&offset=:offset
     */
    public function actionIndex() {
        $filters = trim(Yii::$app->request->get('username'));
        if(strlen($filters)) {
            $filters = array_map(function ($v) {
                    return trim($v);
                },
                array_values(array_filter(explode(' ', $filters), function ($v) {
                    return strlen(trim($v));
                })));
        } else
            $filters = [];

        if(count($filters)) {
            $users = User::find()
                        // find users by name, except this user
                        ->where(['!=', 'id', Yii::$app->user->identity->id])
                        ->andWhere(['like', 'CONCAT_WS(" ", name, surname)', $filters])
                        ->offset((int)Yii::$app->request->get('offset'))
                        ->limit(100)
                        // DESC using for refresh search response
                        // because, when we using ASC, so always we get equal response(from 1 to 100)
                        // but with asc we get newest users, which register recently
                        ->orderBy('id DESC')
                        ->all();
            return ['users' => $users];
        } else
            return ['users' => []];
    }

    /**
     * @api {get} /users/top?offset=:offset Get users top
     * @apiGroup User Actions
     * @apiName users-top
     * @apiDescription Method get a users, which have more dives during last month(30 days). Attention!!! Method receive information about max 100 users. For receive info more then 100 users, please use field 'offset'.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     * @apiParam {number} offset Offset of users(when, you receive info fot first 100 users, then you must pass param 'offset' = 100 in next request)
     *
     * @apiSuccess {Object} users collection Collection of top users
     *
     * @apiSuccessExample Success Response Example
     *  {
     *      "users": [
     *          {
     *              "id": 3,
     *              "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *          }
     *      ],
     *      "success": true
     *  }
     *
     * @apiSampleRequest /users/top?offset=:offset
     */
    public function actionTop() {
        $users = (new Query)->select(['users.id', 'users.name', 'users.surname', 'avatar' => 'CONCAT("www.", "'.Yii::$app->params['domainName'].'", "/", users.avatar)'])
                            ->from('users')
                            ->innerJoin('dives')
                            ->where(['dives.user_id'=>new Expression('users.id')])
                            ->andWhere(['<=', 'dives.date', new Expression('NOW()')])
                            ->andWhere(['>=', 'dives.date', new Expression('NOW() - INTERVAL 1 MONTH')])
                            ->groupBy('dives.user_id')
                            ->having(['>', new Expression('COUNT(*)'), 0])
                            ->orderBy(new Expression('COUNT(*) DESC'))
                            ->offset((int)Yii::$app->request->get('offset'))
                            ->limit(100)
                            ->all();
        return ['users' => $users];
    }

    /**
     * @api {post} /user/attach-facebook-login Attach facebook login
     * @apiGroup User Actions
     * @apiName attach-facebook-login
     * @apiDescription Method will attach a user's facebook login for search friends via facebook
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {string} access_token contains special access token received from facebook. More information(search access_tocken): <a href="https://developers.facebook.com/docs/facebook-login/access-tokens/expiration-and-extension">https://developers.facebook.com/docs/facebook-login/access-tokens/expiration-and-extension</a>
     * @apiError 400024 You cannot attach facebook login for user, which registered via facebook
     * @apiSampleRequest /user/attach-facebook-login
     */
    public function actionAttachFacebook() {
        $user = Yii::$app->user->identity;
        if($user->registration_type == 'facebook')
            return ['errors' => ['facebook' => [400024]]];

        try {
            $form = new LoginFacebookForm(Yii::$app->request->post());
        } catch(UnknownPropertyException $e) {
            return ['errors' => ['general' => [900002]]];
        }
        if (!$form->validate())
            return ['errors' => $form->getErrors()];

        $fb = new Facebook(Yii::$app->params['facebook']);

        try {
            $facebook_id = $fb->get(
                '/me?fields=id',
                Yii::$app->request->post('access_token')
            )->getGraphUser()->getId();
        } catch(FacebookResponseException $e) {
            return ['errors' => ['facebook'=>[100018]]];
        } catch(FacebookAuthenticationException $e) {
            return ['errors' => ['facebook'=>[100020]]];
        } catch(FacebookSDKException $e) {
            return ['errors' => ['facebook'=>[100019]]];
        } catch(\Exception $e) {
            return ['errors' => ['facebook'=>[100022]]];
        }


        $user->facebook_id = $facebook_id;
        $user->save(false);
    }

    /**
     * @api {post} /user/detach-facebook-login Detach facebook login
     * @apiGroup User Actions
     * @apiName detach-facebook-login
     * @apiDescription Method will detach a user's facebook login
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     * @apiError 400023 You cannot detach facebook login for user, which registered via facebook
     *
     * @apiSampleRequest /user/detach-facebook-login
     */
    public function actionDetachFacebook() {
        $user = Yii::$app->user->identity;
        if($user->registration_type != 'facebook') {
            $user->facebook_id = null;
            $user->save(false);
        } else {
            return ['errors' => ['facebook' => [400023]]];
        }
    }
}