<?php
namespace frontend\controllers;

use frontend\vendor\YAuth;


class AuthController extends ApiController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => YAuth::className(),
        ];
        return $behaviors;
    }
}