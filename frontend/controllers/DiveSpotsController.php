<?php
namespace frontend\controllers;

use Yii;
use frontend\models\DiveSpot;
use yii\db\Expression;

class DiveSpotsController extends AuthController
{
    /**
     * @api {get} /dive-spots?filter=:filter Dive Spots Search
     * @apiGroup Dive Spots Actions
     * @apiName dive-spots-search
     * @apiDescription Method provide a dive spots information which consist users input
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {text} filter Users input
     * @apiSuccess {Object} spots collection Collection of filtered spots
     * @apiSuccessExample Success Response Example
     * {
     *  "spots": [
     *      {
     *          "id": 2,
     *          "country": "Country",
     *          "region": "Region",
     *          "location": "Location",
     *          "spot_name": "Spot Name",
     *          "lat": 22,
     *          "lng": 44.0003
     *      }
     *  ],
     *  "success": true
     * }
     * @apiSampleRequest /dive-spots?filter=:filter
     */
    public function actionIndex() {
        $filters = Yii::$app->request->get('filter');

        if(strlen($filters)) {
            $filters = array_map(function ($v) {
                return trim($v);
            },
                array_values(array_filter(explode(',', $filters), function ($v) {
                    return strlen(trim($v));
                })));
        } else
            $filters = [];

        if(count($filters)) {
            $spots = DiveSpot::find()
                ->where(['moderated'=>true, 'global'=>true])
                ->andWhere(['like', "CONCAT_WS(', ',country,region,location,spot_name,lat,lng)", $filters])
                ->limit(20)
                ->all();

            return ['spots' => $spots];
        } else {
            return ['spots' => []];
        }
    }

    /**
     * @api {get} /dive-spots/synchronize/:synchronize_time Dive Spots Synchronize
     * @apiGroup Dive Spots Actions
     * @apiName dive-spots-synchronize
     * @apiDescription Method provide a dive spots information for new, edited and deleted spots. All useful info contained in object "spots": 1. if 'deleted' is true - you must delete this spot from local DB; 2. if 'id' absent in a local DB - its a new spot; 3. if 'id' present in a local DB - its a edited spot. After this request, desirable send request for synchronize dives, for update 'spot_id'
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{0-999999999}} synchronize_time A last timestamp for synchronize. For first query please use 0
     * @apiSuccess {Object} spots Collection of synchronized spots
     * @apiSuccess {number} synchronize_time New synchronize time(timestamp)
     * @apiSuccess {bool} fully Maximum limit of a dive spots for the request - 50 dive spots in response. If param is FALSE, then you need send a new synchronize request, with a new synchronize_time
     * @apiSuccessExample Success Response Example
     * {
     *  "spots": [
     *      {
     *          "id": 2,
     *          "country": "Country",
     *          "region": "Region",
     *          "location": "Location",
     *          "spot_name": "Spot Name",
     *          "lat": 22,
     *          "lng": 44.0003
     *      }
     *  ],
     * "fully": true,
     * "synchronize_time": 1455490694,
     * "success": true
     * }
     * @apiSampleRequest /dive-spots/synchronize/:synchronize_time
     */
    public function actionSynchronize($synchronize_time) {
        $spots = DiveSpot::find()
            ->where(['moderated'=>true])
            ->andWhere(['>', 'updated_at', new Expression("FROM_UNIXTIME($synchronize_time)")])
            ->andWhere(['or', ['global'=>true, 'deleted'=>false], ['owner_id' => Yii::$app->getUser()->id]])
            ->orderBy('updated_at')
            ->limit(50)
            ->all();

        // for custom $spot->fields()
        array_walk($spots, function(&$spot) {
            $spot->scenario = 'synchronize';
        });

        return [
            'spots' => $spots,
            'fully' => count($spots) === 50 ? false : true,
            'synchronize_time' => count($spots) > 0 ? (new \DateTime(end($spots)->updated_at))->getTimestamp() : (int)$synchronize_time
        ];
    }
}