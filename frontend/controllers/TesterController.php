<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class TesterController extends Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionFacebook() {
        return $this->render('facebook');
    }
}