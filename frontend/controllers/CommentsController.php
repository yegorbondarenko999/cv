<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Post;
use frontend\models\Comment;

/**
 * CommentsController implements the CRUD actions for Dive model.
 */
class CommentsController extends AuthController
{
    /**
     * @api {get} /post/:post_id/comments Get post's comments
     * @apiGroup Wall Actions
     * @apiName obtain-posts-comments
     * @apiDescription Method receive all post's comments
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} post_id Post id
     *
     * @apiSuccess {Object} comments Collection of comments
     * @apiSuccessExample Success Response Example
     * {
     *  "comments":[
     *      {
     *          "id": 9,
     *          "body": "22df df gasd asasdfa sdf dasf das fsdafs df5",
     *          "author": {
     *              "id": 9
     *           },
     *           "created_at": "2016-03-22 23:47:21"
     *      },
     *      ...
     *  ],
     *  "success": true
     * }
     *
     * @apiError 300013 post_id is not exist
     *
     * @apiSampleRequest /post/:post_id/comments
     */
    public function actionIndex($post_id) {
        $post = Post::findOne((int)$post_id);

        if(is_null($post))
            return ['errors' => ['general' => [300013]]];

        $comments = $post->getComments()
            ->with('author')
            ->orderBy('id ASC')
            ->all();

        $comments_info = [];
        foreach($comments as $comment) {
            $comments_info[] =  $comment->toArray() +
                                ['author' => $comment->author->toArray()];
        }

        return [
            'comments' => $comments_info,
        ];
    }

    /**
     * @api {post} /comment Create comment for post
     * @apiGroup Wall Actions
     * @apiName create-comment
     * @apiDescription Method creates a comment for post.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} post_id id of post
     * @apiParam {string{1-65000}} body comment's body. All spaces at start and at the end will trimmed
     * @apiSuccessExample Success Response Example
     * {
     *  "comment": {
     *      "id": 9,
     *      "body": "22df df gasd asasdfa sdf dasf das fsdafs df5",
     *      "author": {
     *          "id": 9
     *      },
     *      "created_at": "2016-03-22 23:47:21"
     *  },
     *  "success": true
     * }
     *
     * @apiError 300012 post_id is required
     * @apiError 300006 post_id is not exist
     * @apiError 300008 body is required
     * @apiError 300009 body is not string
     * @apiError 300010 body is too short
     * @apiError 300011 body is too long
     *
     * @apiSampleRequest /comment
     */
    public function actionCreate()
    {
        $comment = new Comment(Yii::$app->request->post());
        $comment->scenario ='create';
        $comment->author_id = Yii::$app->user->identity->id;
        if($comment->validate() && $comment->save())
            return ['post' =>
                        Comment::findOne($comment->id)->toArray() +
                        ['author' => Yii::$app->user->identity->toArray()]
            ];
        else
            return ['errors' => $comment->getErrors()];
    }

    /**
     * @api {delete} /comment/:id Delete comment
     * @apiGroup Wall Actions
     * @apiName delete-comment
     * @apiDescription Method will delete comment
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number} id Comment id
     *
     * @apiError 300014 the id not exist in db / not belongs to user / not belongs to user's post
     *
     * @apiSampleRequest /comment/:id
     */
    public function actionDelete($id) {
        $comment = Comment::find()
                        ->where(['id'=>$id])
                        ->andWhere([
                            'or',
                            ['author_id' => Yii::$app->getUser()->id],
                            ['in', 'post_id', array_column(Yii::$app->user->identity->getPosts()->asArray()->all(), 'id')]
                        ])
                        ->one();

        if(isset($comment))
            $comment->delete();
        else
            return ['errors' => ['comment' => [300014]]];
    }
}