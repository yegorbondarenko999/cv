<?php

namespace frontend\controllers;

use Yii;
use frontend\models\SubscribeForm;
use frontend\models\UnsubscribeForm;
use frontend\models\User;

class SubscriptionsController extends AuthController
{
    /**
     * @api {post} /user/:user_id/subscribe Subscribe to user
     * @apiGroup Followers Actions
     * @apiName subscribe
     * @apiDescription Method make a subscription a for user.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{1-999999999}} user_id A target users id, on which current user want to subscribe
     *
     * @apiError 500001 user_id is a required
     * @apiError 500002 user with user_id is not exist
     * @apiError 500003 user with user_id in your subscription already
     *
     * @apiSampleRequest /user/:user_id/subscribe
     */
    public function actionSubscribe($user_id) {
        $form = new SubscribeForm(['user_id' => $user_id]);
        if($form->validate())
            Yii::$app->user->identity->link('subscriptions', User::findOne(['id'=>$user_id]));
        else
            return ['errors' => $form->getErrors()];
    }

    /**
     * @api {post} /user/:user_id/unsubscribe Unsubscribe to user
     * @apiGroup Followers Actions
     * @apiName unsubscribe
     * @apiDescription Method remove a subscription a for user.
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     * @apiParam {number{1-999999999}} user_id A target users id, on which current user want to subscribe
     *
     * @apiError 500004 user with user_id not in your subscription yet
     *
     * @apiSampleRequest /user/:user_id/unsubscribe
     */
    public function actionUnsubscribe($user_id) {
        $form = new UnsubscribeForm(['user_id' => $user_id]);
        if($form->validate())
            Yii::$app->user->identity->unlink('subscriptions', User::findOne(['id'=>$user_id]), true);
        else
            return ['errors' => $form->getErrors()];
    }


    /**
     * @api {get} /subscriptions Get user's subscriptions
     * @apiGroup Followers Actions
     * @apiName subscriptions
     * @apiDescription Method get users subscription's
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     *@apiSuccessExample Success Response Example
     * {
     *      "subscriptions": [
     *          {
     *              "id": 1,
     *              "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *          }
     *      ],
     *      "success": true
     * }
     *
     * @apiSampleRequest /subscriptions
     */
    public function actionIndex() {
        return ['subscriptions' => Yii::$app->user->identity->subscriptions];
    }
}