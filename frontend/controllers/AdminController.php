<?php
namespace frontend\controllers;

use yii\web\Controller;
use Yii;
use frontend\models\AdminLoginForm;

class AdminController extends Controller
{
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/unmoderated-spots');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}