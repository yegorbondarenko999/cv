<?php

namespace frontend\controllers;

use Yii;

class FollowersController extends AuthController
{
    /**
     * @api {get} /followers Get user's followers
     * @apiGroup Followers Actions
     * @apiName followers
     * @apiDescription Method get user's followers
     * @apiHeader {string{32}} Authorization Special authentication key. String MUST start from "Bearer". E.g.: Bearer vsy0W_Jt4BJjOWd0tdfDuT76ZDDSEVJI
     *
     *@apiSuccessExample Success Response Example
     * {
     *      "followers": [
     *          {
     *              "id": 1,
     *              "avatar": null,
     *              "name": "q",
     *              "surname": "q"
     *          }
     *      ],
     *      "success": true
     * }
     *
     * @apiSampleRequest /followers
     */
    public function actionIndex() {
        return ['followers' => Yii::$app->user->identity->followers];
    }
}