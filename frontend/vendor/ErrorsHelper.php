<?php
namespace frontend\vendor;

use yii;

/*
 * costil for translate errors for site
 * NEVER send code-errors via api! ONLY text!
 * Because fo fix translate mistake, we must change text only on the server,
 * but not create new update for app*/
class ErrorsHelper {

    private static $e = [
        '200039' => 'The field "Country" is required, if not selected existing spot',
        '200040' => 'The field "Country" must be a string',
        '200041' => 'The field "Country" is too short',
        '200042' => 'The field "Country" is too long',
        '200043' => 'The field "Region" is required, if not selected existing spot',
        '200044' => 'The field "Region" must be a string',
        '200045' => 'The field "Region" is too short',
        '200046' => 'The field "Region" is too long',
        '200047' => 'The field "Location" is required, if not selected existing spot',
        '200048' => 'The field "Location" must be a string',
        '200049' => 'The field "Location" is too short',
        '200050' => 'The field "Location" is too long',
        '200051' => 'The field "Spot name" is required, if not selected existing spot',
        '200052' => 'The field "Spot name" must be a string',
        '200053' => 'The field "Spot name" is too short',
        '200054' => 'The field "Spot name" is too long',
        '200055' => 'The field "Latitude" is required, if not selected existing spot',
        '200056' => 'The field "Latitude" must be a number',
        '200057' => 'The field "Latitude" is too small',
        '200058' => 'The field "Latitude" is too big',
        '200059' => 'The field "Longitude" is required, if not selected existing spot',
        '200060' => 'The field "Longitude" must be a number',
        '200061' => 'The field "Longitude" is too small',
        '200062' => 'The field "Longitude" is too big',
        '200063' => 'The "Existing spot" not exist in db. Maybe it\'s deleted recently',
    ];

    public static function setRulesTranslate($rules) {
        return array_map(function($rule) {
            if(isset($rule['message']))
                $rule['message'] = self::getTranslateByCode($rule['message']);
            if(isset($rule['tooShort']))
                $rule['tooShort'] = self::getTranslateByCode($rule['tooShort']);
            if(isset($rule['tooLong']))
                $rule['tooLong'] = self::getTranslateByCode($rule['tooLong']);
            if(isset($rule['tooSmall']))
                $rule['tooSmall'] = self::getTranslateByCode($rule['tooSmall']);
            if(isset($rule['tooBig']))
                $rule['tooBig'] = self::getTranslateByCode($rule['tooBig']);

            return $rule;
        }, $rules);

    }

    public static function getTranslateByCode($code) {
        return  strpos(Yii::$app->request->absoluteUrl, '/admin/unmoderated-spots') !== false ? self::$e[$code] : $code;
    }
}