<?php
namespace frontend\vendor;

use yii\web\UploadedFile;

trait Saveble {

    private $directory_chmod = 0755;
    private $file_chmod = 0644;
    //protected $public_directory;

    private function getDirectory() {

        if(!isset($this->public_directory))
            throw new \Exception('no setting public directory for model');

        //здесь эта проверка необходима, для того, чтобы я не начал
        //делать папки неизвестно где, а были только жестко забитые,
        //которые создаются при переносе движка
        if(!file_exists($this->public_directory))
            throw new \Exception('public directory not created yet');

        /*
         * количество элементов в директории*/
        $limit_in_directory = 999;
        /*
         * указав эту глубину вложенности дирректорий можно посчитать сколько файлом может поместится всего
         * учитывая что в одной папке может быть $limit_in_directory папок или файлов, значит, если в конечных
         * папках будут только файлы, а во всех остальных только папки, значит файлов всего $limit_in_directory^$max_depth+1
         * то есть в случае для 999 значит 997002999 файлов*/
        $max_depth = 2;

        $depth = 0;

        while(true) {
            $directories = scandir($this->public_directory,SCANDIR_SORT_DESCENDING);

            /*
             * проверяем, находимся ли мы на максимальной допустимой глубине
             * эта проверка в себе несет такой смысл, если мы уже в конечной папке, тогда мы проверяем, не превышен ли лимит файлов
             * в этой папке. На всех предыдущих папках я этого не делаю, т.к. например лимит элементов может быть в папке 2-а, и тогда
             * если в корневой папке будет 1-а папка заполенная под завязку, то при создании второй папки в следующих операциях скрипт
             * будет считать что в корневой папке уже забито все по максимуму, т.к. там уже 2-е папки из 2-х и появится ошибка, что
             * мемори лимит исчерпан. Именно поэтому я проверяю если максимальная глубина, то только тогда проверять на лимит*/
            if($depth === $max_depth) {
                /*
                 * если глубина максимальная, то проверяю, достигнут ли лимит в директории
                 * +2 потому что scandir так же показывает "." (текущая папка) и ".." (предыдущая папка)
                 * если лимита нет, то мы достигли чего хотел, и текущая папка и есть та, куда мы будем все лить
                 * в противном случае в цикле буем брать предыдущую директорию, проверять, если там нет лимита,
                 * то тогда создаем новую папку и эта папка будет конечной, иначе опускаемся еще на уровень инже и тд*/
                if(count($directories) >= $limit_in_directory+2) {
                    do {
                        /*
                         * проверяем, если в корневой директории превышен лимит на элементы, то значит что пользователи
                         * залили несколько миллиардов файлоф, а значит есть теперь время думать о подключении новых серваков*/
                        if($depth < 0)
                            throw new \Exception('limit in root directory');

                        $depth--;
                        //берем предыдущую директорию и сканируем в ней количество элементов
                        $this->public_directory = substr($this->public_directory,0,strripos($this->public_directory, '/'));
                        $directories = scandir($this->public_directory,SCANDIR_SORT_DESCENDING);

                        /*
                         * проверяем, не превышен ли лимит на файлы сейчас. В данном случае если всего может быть 999
                         * элементов в директории, то если их на данный момент 999, значит новую директорию мы создать уже не можем
                         * а значит непобходимо перейти на директорию еще уровнем ниже*/
                        if(count($directories) >= $limit_in_directory+2)
                            continue;


                        /*
                         * создаем новую директорию с названием на единицу большей, чем предыдущая*/
                        $new_directory = $this->public_directory.'/1';
                        //если это не первый каталог в директории, то ищем последний каталог в этой директории и создаем следующий
                        //с названием на один больше
                        for($i=0;$i<count($directories);$i++) {
                            if( $directories[$i] !== '.' and
                                $directories[$i] !== '..' and
                                is_dir($this->public_directory .'/'. $directories[$i])===true) {
                                $new_directory = $this->public_directory .'/'. ((int)$directories[$i]+1);
                                break;
                            }
                        }

                        //проверяем на существование каталога, т.к. если скрипты паралельно запустятся
                        //то тут будет эксепшени получается что скрипт может не продолжиться дальше
                        if(is_dir($new_directory) === false)
                            if(!mkdir($new_directory, $this->directory_chmod, true))
                                throw new \Exception;
                        /*
                         * таким образом я получил новенькую директорию, где в будущем будут или заливаться файлы, или создаваться
                         * новые директории, поэтому я выхожу из цикла для создания директории в предыдущем(их) каталоге(ах),
                         * и снова начинаю цикл для поиска конечной директории для заливки файла */
                        $this->public_directory = $new_directory;
                        $depth++;
                        break;

                    } while(true);
                    continue;
                } else
                    break;
            }

            /*
             * получаем последнюю директорию в текущей директории, и если ее нет, то создаем новую, увеличиваем глубину на один и
             * начинаем все с начала*/
            $last_directory = $this->public_directory.'/1';
            for($i=0;$i<count($directories);$i++) {
                if( $directories[$i] !== '.' and
                    $directories[$i] !== '..' and
                    is_dir($this->public_directory .'/'. $directories[$i])===true) {
                    $last_directory = $this->public_directory.'/'. $directories[$i];
                    break;
                }
            }

            if(is_dir($last_directory) === false)
                if(!mkdir($last_directory,$this->directory_chmod,true))
                    throw new \Exception;

            $this->public_directory = $last_directory;
            $depth++;
        }

        return $this->public_directory.'/';
    }

    //protected function saveFile(UploadedFile $file) {
    protected function saveFile($tmp_filepath) {
        $directory = $this->getDirectory();
        try {
            do {
                $filename = md5(microtime() . rand(0, 9999));
                $filepath = $directory . $filename;
            } while (file_exists($filepath));

            rename(
                $tmp_filepath,
                $filepath
            );
            chmod($filepath, $this->file_chmod);
            return $filepath;
        } catch(\Exception $e) {
            if(isset($filepath) && file_exists($filepath))
                unlink($filepath);
            return false;
        }
    }
}
