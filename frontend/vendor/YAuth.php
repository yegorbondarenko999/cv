<?php

namespace frontend\vendor;

use Yii;
use yii\web\Response;
use \yii\filters\auth\HttpBearerAuth;
use yii\web\UnauthorizedHttpException;

class YAuth extends HttpBearerAuth
{
    public function beforeAction($action)
    {
        $response = $this->response ? : Yii::$app->getResponse();

        try {
            $identity = $this->authenticate(
                $this->user ?: Yii::$app->getUser(),
                $this->request ?: Yii::$app->getRequest(),
                $response
            );
        } catch (UnauthorizedHttpException $e) {}

        if (isset($identity)) {
            return true;
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = [
                'success' => false,
                'errors' => [900001]
            ];
            Yii::$app->response->send();
            return false;
        }
    }
}