<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\BootstrapAsset;
use yii\widgets\LinkPager;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
?>
<?php
$this->title = 'Unmoderated spots';
?>

<div class="table-striped">
    <div class="thead">
        <div class="row row-padding-s">
            <div class="col-xs-2">
                <div class="row row-padding-s">
                    <div class="col-xs-12">Choose existing</div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row row-padding-s">
                    <div class="col-xs-3">Country</div>
                    <div class="col-xs-3">Region</div>
                    <div class="col-xs-3">Location</div>
                    <div class="col-xs-3">Spot name</div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row row-padding-s">
                    <div class="col-xs-2">Global</div>
                    <div class="col-xs-3">Latitude</div>
                    <div class="col-xs-3">Longitude</div>
                    <div class="col-xs-4">Actions</div>
                </div>
            </div>
        </div>
    </div>
    <div class="tbody">
    <?php foreach($spots as $spot) { ?>
        <div class="row row-padding-s">
        <?php $form = ActiveForm::begin([
            'method' => 'PUT',
            'validateOnSubmit' => false,
            'action' =>  Url::to(['admin-dive-spots/unmoderated-accept', 'id' => $spot->id]),
            'options' => [
                'class' => 'ajax-form',
                'data-object' => 1,
                'data-callback' => 'removeSpot',
                'data-before-action' => 'setWaitStatus'
            ]
        ]); ?>
            <div class="col-xs-2">
                <div class="row row-padding-s">
                    <div class="col-xs-12"><?=
                        AutoComplete::widget([
                            'options' => [
                                'form' => $form->getId(),
                                'class' => 'form-control'
                            ],
                            'clientOptions' => [
                                'source' => new JsExpression("
                                    function (request, response) {
                                        $.ajax({
                                            url: '".(Url::to(['admin-dive-spots/index']))."',
                                            type: 'GET',
                                            data: {
                                                filter: request.term,
                                                autocomplete: true
                                            },
                                            success: function(data) {
                                                response(data.spots);
                                            },
                                            error: function() {
                                                console.log('autocomplete error');
                                            }
                                        });
                                    }
                                ")
                            ],
                            'clientEvents' => [
                                'select' => <<<EOT
                                    function (e, ui) {
                                        var inputs = $(this).closest('form').find('input[type=text], input[type=checkbox]').not($(this));
                                        inputs.attr('readonly', true).addClass('ignore-helpers').siblings('p').addClass('hidden');

                                        // we need set "prev-value" for check, if user change input before action "focusout"
                                        $(this).data('prev-value', ui.item.value).data('original-id', ui.item.id);
                                    }
EOT
                            ],
                        ]);
                        ?>
                        <input name="original-id" type="hidden" form="<?=$form->getId()?>"/>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row row-padding-s">
                    <div class="col-xs-3"><?= $form->field($spot, 'country')->label(false) ?></div>
                    <div class="col-xs-3"><?= $form->field($spot, 'region')->label(false) ?></div>
                    <div class="col-xs-3"><?= $form->field($spot, 'location')->label(false) ?></div>
                    <div class="col-xs-3"><?= $form->field($spot, 'spot_name')->label(false) ?></div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="row row-padding-s">
                    <div class="col-xs-2 text-center"><?= $form->field($spot, 'global', ['template'=>"<div class='checkbox-inline'>{input}</div>"])->checkbox([], false)->label(false) ?></div>
                    <div class="col-xs-3"><?= $form->field($spot, 'lat')->label(false) ?></div>
                    <div class="col-xs-3"><?= $form->field($spot, 'lng')->label(false) ?></div>
                    <div class="col-xs-4"><?= Html::submitButton('Accept', ['class' => 'btn btn-primary', 'data-loading-text' => "Loading...", 'name' => 'accept-button']) ?></div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    <?php } ?>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>

<?php
$this->registerCssFile("/css/spots.css", [
    'depends' => [BootstrapAsset::className()],
]);
$this->registerJsFile('/js/ylib.js', [
    'depends' => ['\yii\web\JqueryAsset'],
    'position'=>View::POS_END
]);
$this->registerJsFile('/js/spots.js', [
    'depends' => ['\yii\web\JqueryAsset'],
    'position'=>View::POS_END
]);
?>