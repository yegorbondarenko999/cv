<?php

namespace common\modules\auth\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionCreateAssigment() {
        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $auth->assign($admin, 1);
    }

    public function actionCreateRoles()
    {
        $auth = Yii::$app->authManager;
        $index_unmoderated_spots = $auth->createPermission('admin-dive-spots/unmoderated-list');
        $accept_unmoderated_spot = $auth->createPermission('admin-dive-spots/unmoderated-accept');
        $search_spots = $auth->createPermission('admin-dive-spots/index');

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $index_unmoderated_spots);
        $auth->addChild($admin, $accept_unmoderated_spot);
        $auth->addChild($admin, $search_spots);
    }

    public function actionCreatePermissions()
    {
        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $index_unmoderated_spots = $auth->createPermission('admin-dive-spots/unmoderated-list');
        $index_unmoderated_spots->description = 'access to index unmoderated spots';
        $auth->add($index_unmoderated_spots);

        // add "updatePost" permission
        $accept_unmoderated_spot = $auth->createPermission('admin-dive-spots/unmoderated-accept');
        $accept_unmoderated_spot->description = 'accept unmoderated spot';
        $auth->add($accept_unmoderated_spot);

        // add "updatePost" permission
        $search_spots = $auth->createPermission('admin-dive-spots/index');
        $search_spots->description = 'search dive spots';
        $auth->add($search_spots);
    }
}
