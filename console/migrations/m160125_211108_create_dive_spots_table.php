<?php
use yii\db\Migration;

class m160125_211108_create_dive_spots_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dive_spots}}', [
            'id' => $this->primaryKey(),
            'country' => $this->string()->notNull(),
            'region' => $this->string()->notNull(),
            'location' => $this->string()->notNull(),
            'spot_name' => $this->string()->notNull(),
            'lat' => $this->decimal(6,4)->notNull(),
            'lng' => $this->decimal(7,4)->notNull(),
            'moderated' => $this->boolean()->notNull()->defaultValue(false),
            // spot can be moderated, but not global!
            'global' => $this->boolean()->notNull()->defaultValue(false),
            // if global = false, then for sync we check, to which user we pass this spot
            'owner_id' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false),
            // old mysql, so no $this->timestamp()->notNull()
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('dive_spots', 'dives', 'spot_id', 'dive_spots', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('dive_spot_owner', 'dive_spots', 'owner_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('dive_spots', 'dives');
        $this->dropForeignKey('dive_spot_owner', 'dive_spots');
        $this->dropTable('{{%dive_spots}}');
    }
}
