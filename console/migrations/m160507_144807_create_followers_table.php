<?php

use yii\db\Migration;

class m160507_144807_create_followers_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%followers}}', [
            'id' => $this->primaryKey(),
            'target_id' => $this->integer()->notNull(),
            'follower_id' => $this->integer()->notNull(),
            'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ], $tableOptions);

        $this->createIndex('followers_unique', 'followers', ['follower_id', 'target_id'], true);
        $this->addForeignKey('users_followers', 'followers', 'follower_id', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('users_targets', 'followers', 'target_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('users_followers', 'followers');
        $this->dropForeignKey('users_targets', 'followers');
        $this->dropTable('{{%followers}}');
    }
}
