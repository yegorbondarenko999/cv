<?php
use yii\db\Migration;

class m151217_204813_create_dives_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dives}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'spot_id' => $this->integer()->notNull(),
            'conditions' => "ENUM('standard', 'nightly', 'drift', 'under_ice', 'deep', 'wreck', 'mountain', 'free', 'cave', 'training')",
            // [{'type':'nitrox30', 'volume':12, 'pressure':300},{}]
            'balloons' => $this->text(),
            //meters
            'depth' => $this->decimal(5,2),
            'dive_time' => $this->integer(),
            //kg
            'ballast' => $this->decimal(5,2),
            //celsius
            'temperature_start' => $this->smallInteger(),
            'temperature_end' => $this->smallInteger(),
            'measurement_system' =>  "ENUM('metric', 'imperial')",
            'suit' => "ENUM('dry', 'wet', 'short', 'other')",
            'visibility' => $this->smallInteger(),
            //1-5
            'rating' => $this->smallInteger(),
            'note' => $this->text(),
            'partner_id' => $this->integer(),
            'primary_id' => $this->integer(),
            'confirmed' => $this->boolean()->notNull()->defaultValue(true),
            'deleted' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('users_dives', 'dives', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('partners_dives', 'dives', 'partner_id', 'users', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('users_dives', 'dives');
        $this->dropTable('{{%dives}}');
    }
}
