<?php
use yii\db\Migration;

class m151227_210305_create_dives_images_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dives_images}}', [
            'id' => $this->primaryKey(),
            'dive_id' => $this->integer()->notNull(),
            'url' => $this->string()->unique()->notNull(),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('dives_images', 'dives_images', 'dive_id', 'dives', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('dives_images', 'dives_images');
        $this->dropTable('{{%dives_images}}');
    }
}
