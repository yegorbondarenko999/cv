<?php

use yii\db\Schema;
use yii\db\Migration;

class m160507_203734_create_posts_images_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%posts_images}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'url' => $this->string()->unique()->notNull(),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('posts_images', 'posts_images', 'post_id', 'posts', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('posts_images', 'posts_images');
        $this->dropTable('{{%posts_images}}');
    }
}
