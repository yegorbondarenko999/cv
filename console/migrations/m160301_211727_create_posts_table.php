<?php

use yii\db\Migration;

class m160301_211727_create_posts_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%posts}}', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer()->notNull(),
            'note' => $this->text(),
            'dive_id' => $this->integer(),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('users_posts', 'posts', 'owner_id', 'users', 'id', 'CASCADE', 'CASCADE');
        // бестолковый ключ, потому что дайвы реально в базе не удаляются, а им ставится флаг deleted, для корректной синхронизации
        // оставлю эту пометку, чтобы случайно не начинать добавлять этот ключ
        // $this->addForeignKey('dives_posts', 'posts', 'dive_id', 'dives', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('post_dive_unique', 'posts', 'dive_id', true);
    }

    public function down()
    {
        $this->dropForeignKey('users_posts', 'posts');
        $this->dropTable('{{%posts}}');
    }
}
