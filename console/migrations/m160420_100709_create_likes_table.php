<?php

use yii\db\Migration;

class m160420_100709_create_likes_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%likes}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->createIndex('likes_unique', 'likes', ['author_id', 'post_id'], true);
        $this->addForeignKey('users_likes', 'likes', 'author_id', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('posts_likes', 'likes', 'post_id', 'posts', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('users_likes', 'likes');
        $this->dropForeignKey('posts_likes', 'likes');
        $this->dropTable('{{%likes}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
