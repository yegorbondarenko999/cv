<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->execute("SET time_zone = '+00:00'");

        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            // email or facebook id
            'login' => $this->string(100)->notNull()->unique(),
            'registration_type' => "ENUM('conventional', 'facebook') NOT NULL DEFAULT 'conventional'",
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string()->unique(),
            'name' => $this->string(50)->notNull(),
            'surname' => $this->string(50)->notNull(),
            'date_of_birth' => $this->date(),
            'certificate' => $this->string(30),
            'note' => $this->text(),
            'avatar' => $this->string()->unique(),
            'language' => "ENUM('eng', 'rus', 'ukr') NOT NULL DEFAULT 'eng'",
            'measurement_system' =>  "ENUM('metric', 'imperial') NOT NULL DEFAULT 'metric'",
            'starting_dives_number' => $this->integer()->notNull()->defaultValue(1),
            // it not depend of  registration type. it need for case search
            // people in our system and simultaneously on facebook
            'facebook_id' => $this->bigInteger(),
            'created_at' => 'timestamp NOT NULL DEFAULT "0000-00-00 00:00:00"',
            'updated_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->createIndex('users_unique', 'users', ['login', 'registration_type'], true);
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
